import typescript from '@rollup/plugin-typescript';
import terser from '@rollup/plugin-terser';

export default {
  input: 'src/index.ts',
  output: [
    {
      name: '@nomalism-com/api',
      file: 'dist/index.min.js',
      format: 'umd',
      plugins: [terser()],
      globals: {
        axios: 'axios',
        '@nomalism-com/types': '@nomalism-com/types',
      },
    },
  ],
  plugins: [typescript()],
  external: ['axios', '@nomalism-com/types'],
};
