import axios, { AxiosHeaders, AxiosInstance } from 'axios';

import BankDataClass from './modules/user/bankData';
import ClientClass from './modules/user/clients';
import ClientTypeClass from './modules/user/clientType';
import ChatClass from './modules/stock/chat';
import CommissionerClass from './modules/user/commissioner';
import CountryClass from './modules/user/country';
import DocumentHeaderClass from './modules/supply/documentHeader';
import BillOfLadingClass from './modules/document/billOfLading';
import ProductionOrderClass from './modules/document/productionOrder';
import ProformaClass from './modules/document/proforma';
import PropostaFornecedorClass from './modules/document/propostaFornecedor';
import ProviderCreditNoteFromReturnClass from './modules/document/providerCreditNoteFromReturn';
import ProviderFinancialCreditNoteClass from './modules/document/providerFinancialCreditNote';
import ProviderServiceInvoiceClass from './modules/document/providerServiceInvoice';
import DocumentLineClass from './modules/supply/documentLine';
import DocumentLineAssocClass from './modules/supply/documentLineAssoc';
import DocumentTypeClass from './modules/supply/documentType';
import FavoritesClass from './modules/user/favorites';
import FileClass from './modules/stock/file';
import GoogleSheetsClass from './modules/integration/googleSheets';
import GroupPermissionsClass from './modules/user/groupPermission';
import LanguageClass from './modules/user/language';
import LocationClass from './modules/stock/location';
import LogoutClass from './modules/user/logout';
import MultimediaClass from './modules/documentManagement/multimedia';
import ObservationClass from './modules/integration/observation';
import ObservationTypeClass from './modules/integration/observationType';
import PasswordClass from './modules/user/password';
import PermissionsClass from './modules/user/permission';
import ProductImageClass from './modules/stock/productImage';
import PromotionClass from './modules/stock/promotion';
import PromotionAssocClass from './modules/stock/promotionAssoc';
import ProvidersClass from './modules/user/provider';
import ProviderTypeClass from './modules/user/providerType';
import PurchaseConditionsClass from './modules/user/purchaseCondition';
import ReasonForExemptionClass from './modules/user/reasonForExemption';
import RefreshTokenClass from './modules/user/refreshToken';
import SegmentsAreaClass from './modules/user/segmentsArea';
import SessionsClass from './modules/user/sessions';
import ShippingsClass from './modules/user/shippings';
import StoreOperatorClass from './modules/user/storeOperator';
import SwiftClass from './modules/user/swift';
import TypeOfLocationClass from './modules/stock/typeOfLocation';
import UnitOfMeasureClass from './modules/stock/unitOfMeasure';
import UserPermissionsClass from './modules/user/userPermission';
import UserPositionsClass from './modules/user/userPositions';
import UsersClass from './modules/user/user';
import VatTaxClass from './modules/stock/vatTax';
import VatTaxZoneClass from './modules/stock/vatTaxZone';
import WorkflowClass from './modules/supply/workflow';
import DeliveryMethodsClass from './modules/user/deliveryMethods';
import MaturityDatesClass from './modules/user/maturityDates';
import PaymentMethodsClass from './modules/user/paymentMethods';
import VehiclesClass from './modules/user/vehicles';
import ExternalDocumentTypeClass from './modules/supply/externalDocumentType';
import DocumentSetClass from './modules/supply/documentSet';
import PaymentClass from './modules/supply/payment';
import ExternalDocumentHeaderClass from './modules/supply/externalDocumentHeader';
import ClientCurrentAccountClass from './modules/supply/clientCurrentAccount';
import ProviderCurrentAccountClass from './modules/supply/providerCurrentAccount';
import VatValidationClass from './modules/supply/vatValidation';
import StockMovementClass from './modules/stock/stockMovement';
import ZipCodeClass from './modules/user/zipCode';
import TenantClass from './modules/user/tenant';
import PreSaleClass from './modules/supply/preSale';
import PreSaleProductClass from './modules/supply/preSaleProduct';
import OrderManagementClass from './modules/supply/orderManagement';
import NpcClass from './modules/print/npc';
import PrinterClass from './modules/print/printer';
import SchedulePrintJobClass from './modules/print/schedulePrintJob';
import QueryListClass from './modules/stock/queryList';
import QueryParameterClass from './modules/stock/queryParameter';
import ReturnReasonClass from './modules/stock/returnReason';
import PropostaSheetsClass from './modules/document/propostaSheets';
import ScheduleClass from './modules/stock/schedule';
import GoogleFilePermissionClass from './modules/integration/googleFilePermission';
import SettingsClass from './modules/integration/settings';
import TicketsClass from './modules/tickets/tickets';
import ChannelClass from './modules/tickets/channel';
import TicketsLanguageClass from './modules/tickets/language';
import CltClass from './modules/tickets/clt';
import StartDocumentHeaderLastUpdateClass from './modules/supply/startDocumentHeaderLastUpdate';
import PersonaClass from './modules/user/persona';
import ProjectInfoClass from './modules/integration/projectInfo';
import OrderClass from './modules/document/order';
import PurchaseClass from './modules/document/purchase';
import MaterialEntranceClass from './modules/document/materialEntrance';
import TransformadoClass from './modules/document/transformado';
import UpfrontReturnClass from './modules/document/upfrontReturn';
import SavedEmPickingClass from './modules/stock/savedEmPicking';
import EmailTemplateClass from './modules/integration/emailTemplate';
import EmailTemplateAttachmentClass from './modules/integration/emailTemplateAttachment';
import PrisonClass from './modules/stock/prison';
import QuebraClass from './modules/document/quebra';
import InventarioClass from './modules/document/inventario';
import ReturnToProviderClass from './modules/document/returnToProvider';
import EmailVerificationClass from './modules/integration/emailVerification';
import EmailLogClass from './modules/integration/emailLog';
import DocumentLineNoteClass from './modules/supply/documentLineNote';
import SavedProviderProposalClass from './modules/stock/savedProviderProposal';
import ProductGoogleSheetsClass from './modules/stock/productGoogleSheets';
import TaskClass from './modules/stock/task';
import TaskMessageClass from './modules/stock/taskMessage';
import RecurrentTasksClass from './modules/stock/recurrentTasks';
import TaskReadClass from './modules/stock/taskRead';
import ThemeClass from './modules/user/theme';
import DashboardClass from './modules/stock/dashboard';
import ChatRapidMessageClass from './modules/stock/chatRapidMessage';
import SideMenuClass from './modules/stock/sideMenu';
import ErrorLogClass from './modules/view/errorLog';

export * as BankData from './modules/user/bankData';
export * as Client from './modules/user/clients';
export * as ClientType from './modules/user/clientType';
export * as Commissioner from './modules/user/commissioner';
export * as Country from './modules/user/country';
export * as DocumentHeader from './modules/supply/documentHeader';
export * as DocumentLine from './modules/supply/documentLine';
export * as DocumentLineAssoc from './modules/supply/documentLineAssoc';
export * as DocumentType from './modules/supply/documentType';
export * as Favorites from './modules/user/favorites';
export * as File from './modules/stock/file';
export * as GoogleSheets from './modules/integration/googleSheets';
export * as GroupPermissions from './modules/user/groupPermission';
export * as Language from './modules/user/language';
export * as Location from './modules/stock/location';
export * as Logout from './modules/user/logout';
export * as Multimedia from './modules/documentManagement/multimedia';
export * as Observation from './modules/integration/observation';
export * as ObservationType from './modules/integration/observationType';
export * as Password from './modules/user/password';
export * as Permissions from './modules/user/permission';
export * as ProductImage from './modules/stock/productImage';
export * as Promotion from './modules/stock/promotion';
export * as PromotionAssoc from './modules/stock/promotionAssoc';
export * as Providers from './modules/user/provider';
export * as ProviderType from './modules/user/providerType';
export * as PurchaseConditions from './modules/user/purchaseCondition';
export * as ReasonForExemption from './modules/user/reasonForExemption';
export * as RefreshToken from './modules/user/refreshToken';
export * as SegmentsArea from './modules/user/segmentsArea';
export * as Sessions from './modules/user/sessions';
export * as Shippings from './modules/user/shippings';
export * as StoreOperator from './modules/user/storeOperator';
export * as Swift from './modules/user/swift';
export * as TypeOfLocation from './modules/stock/typeOfLocation';
export * as UnitOfMeasure from './modules/stock/unitOfMeasure';
export * as UserPermissions from './modules/user/userPermission';
export * as UserPositions from './modules/user/userPositions';
export * as Users from './modules/user/user';
export * as VatTax from './modules/stock/vatTax';
export * as VatTaxZone from './modules/stock/vatTaxZone';
export * as Workflow from './modules/supply/workflow';
export * as DeliveryMethods from './modules/user/deliveryMethods';
export * as MaturityDates from './modules/user/maturityDates';
export * as PaymentMethods from './modules/user/paymentMethods';
export * as Vehicles from './modules/user/vehicles';
export * as ExternalDocumentType from './modules/supply/externalDocumentType';
export * as DocumentSet from './modules/supply/documentSet';
export * as Payment from './modules/supply/payment';
export * as ExternalDocumentHeader from './modules/supply/externalDocumentHeader';
export * as ClientCurrentAccount from './modules/supply/clientCurrentAccount';
export * as ProviderCurrentAccount from './modules/supply/providerCurrentAccount';
export * as VatValidation from './modules/supply/vatValidation';
export * as StockMovement from './modules/stock/stockMovement';
export * as ZipCode from './modules/user/zipCode';
export * as Tenant from './modules/user/tenant';
export * as PreSale from './modules/supply/preSale';
export * as PreSaleProduct from './modules/supply/preSaleProduct';
export * as OrderManagement from './modules/supply/orderManagement';
export * as Npc from './modules/print/npc';
export * as Printer from './modules/print/printer';
export * as SchedulePrintJob from './modules/print/schedulePrintJob';
export * as QueryList from './modules/stock/queryList';
export * as QueryParameter from './modules/stock/queryParameter';
export * as ReturnReason from './modules/stock/returnReason';
export * as PropostaSheets from './modules/document/propostaSheets';
export * as Schedule from './modules/stock/schedule';
export * as GoogleFilePermission from './modules/integration/googleFilePermission';
export * as Settings from './modules/integration/settings';
export * as Tickets from './modules/tickets/tickets';
export * as Channel from './modules/tickets/channel';
export * as TicketsLanguage from './modules/tickets/language';
export * as Clt from './modules/tickets/clt';
export * as StartDocumentHeaderLastUpdate from './modules/supply/startDocumentHeaderLastUpdate';
export * as Persona from './modules/user/persona';
export * as ProjectInfo from './modules/integration/projectInfo';
export * as Order from './modules/document/order';
export * as Purchase from './modules/document/purchase';
export * as MaterialEntrance from './modules/document/materialEntrance';
export * as Transformado from './modules/document/transformado';
export * as UpfrontReturn from './modules/document/upfrontReturn';
export * as SavedEmPicking from './modules/stock/savedEmPicking';
export * as EmailTemplate from './modules/integration/emailTemplate';
export * as EmailTemplateAttachment from './modules/integration/emailTemplateAttachment';
export * as Prison from './modules/stock/prison';
export * as Quebra from './modules/document/quebra';
export * as Inventario from './modules/document/inventario';
export * as ReturnToProvider from './modules/document/returnToProvider';
export * as EmailVerification from './modules/integration/emailVerification';
export * as EmailLog from './modules/integration/emailLog';
export * as DocumentLineNote from './modules/supply/documentLineNote';
export * as SavedProviderProposal from './modules/stock/savedProviderProposal';
export * as ProductGoogleSheets from './modules/stock/productGoogleSheets';
export * as Task from './modules/stock/task';
export * as TaskMessage from './modules/stock/taskMessage';
export * as RecurrentTasks from './modules/stock/recurrentTasks';
export * as TaskReadClass from './modules/stock/taskRead';
export * as ThemeClass from './modules/user/theme';
export * as DashboardClass from './modules/stock/dashboard';
export * as ChatRapidMessageClass from './modules/stock/chatRapidMessage';
export * as SideMenuClass from './modules/stock/sideMenu';
export * as ErrorLogClass from './modules/view/errorLog';

export type IEnvironment = 'production' | 'uat' | 'development' | 'localhost';

type IService = 'stock' | 'users' | 'integration' | 'documents' | 'view' | 'print' | 'tickets';

export interface IOptions {
  processEnvironment: IEnvironment;
  services: Record<IService, string>;
  gatewayUrl?: string;
  apikey?: string;
  tokenBearer?: string;
}

export interface IModuleConstructor {
  api: AxiosInstance;
  route: string;
  publicRoute: string;
}

export class API {
  public processEnvironment: IEnvironment;

  public client: AxiosInstance;

  public services: Record<IService, string>;

  public BankData: BankDataClass;

  public Client: ClientClass;

  public ClientType: ClientTypeClass;

  public Chat: ChatClass;

  public Commissioner: CommissionerClass;

  public Country: CountryClass;

  public DocumentHeader: DocumentHeaderClass;

  public BillOfLading: BillOfLadingClass;

  public ProductionOrder: ProductionOrderClass;

  public Proforma: ProformaClass;

  public PropostaFornecedor: PropostaFornecedorClass;

  public ProviderCreditNoteFromReturn: ProviderCreditNoteFromReturnClass;

  public ProviderFinancialCreditNote: ProviderFinancialCreditNoteClass;

  public ProviderServiceInvoice: ProviderServiceInvoiceClass;

  public DocumentLine: DocumentLineClass;

  public DocumentLineAssoc: DocumentLineAssocClass;

  public DocumentType: DocumentTypeClass;

  public Favorites: FavoritesClass;

  public File: FileClass;

  public GoogleSheets: GoogleSheetsClass;

  public GroupPermissions: GroupPermissionsClass;

  public Language: LanguageClass;

  public Location: LocationClass;

  public Logout: LogoutClass;

  public Multimedia: MultimediaClass;

  public Observation: ObservationClass;

  public ObservationType: ObservationTypeClass;

  public Password: PasswordClass;

  public Permissions: PermissionsClass;

  public ProductImage: ProductImageClass;

  public Promotion: PromotionClass;

  public PromotionAssoc: PromotionAssocClass;

  public Providers: ProvidersClass;

  public ProviderType: ProviderTypeClass;

  public PurchaseConditions: PurchaseConditionsClass;

  public ReasonForExemption: ReasonForExemptionClass;

  public RefreshToken: RefreshTokenClass;

  public SegmentsArea: SegmentsAreaClass;

  public Sessions: SessionsClass;

  public Shippings: ShippingsClass;

  public StoreOperator: StoreOperatorClass;

  public Swift: SwiftClass;

  public TypeOfLocation: TypeOfLocationClass;

  public UnitOfMeasure: UnitOfMeasureClass;

  public UserPermissions: UserPermissionsClass;

  public UserPositions: UserPositionsClass;

  public Users: UsersClass;

  public VatTax: VatTaxClass;

  public VatTaxZone: VatTaxZoneClass;

  public Workflow: WorkflowClass;

  public DeliveryMethods: DeliveryMethodsClass;

  public MaturityDates: MaturityDatesClass;

  public PaymentMethods: PaymentMethodsClass;

  public Vehicles: VehiclesClass;

  public ExternalDocumentType: ExternalDocumentTypeClass;

  public DocumentSet: DocumentSetClass;

  public Payment: PaymentClass;

  public ExternalDocumentHeader: ExternalDocumentHeaderClass;

  public ClientCurrentAccount: ClientCurrentAccountClass;

  public ProviderCurrentAccount: ProviderCurrentAccountClass;

  public VatValidation: VatValidationClass;

  public StockMovement: StockMovementClass;

  public ZipCode: ZipCodeClass;

  public Tenant: TenantClass;

  public PreSale: PreSaleClass;

  public PreSaleProduct: PreSaleProductClass;

  public OrderManagement: OrderManagementClass;

  public Npc: NpcClass;

  public Printer: PrinterClass;

  public SchedulePrintJob: SchedulePrintJobClass;

  public QueryList: QueryListClass;

  public QueryParameter: QueryParameterClass;

  public ReturnReason: ReturnReasonClass;

  public PropostaSheets: PropostaSheetsClass;

  public Schedule: ScheduleClass;

  public GoogleFilePermission: GoogleFilePermissionClass;

  public Settings: SettingsClass;

  public Tickets: TicketsClass;

  public Channel: ChannelClass;

  public TicketsLanguage: TicketsLanguageClass;

  public Clt: CltClass;

  public StartDocumentHeaderLastUpdate: StartDocumentHeaderLastUpdateClass;

  public Persona: PersonaClass;

  public ProjectInfo: ProjectInfoClass;

  public Order: OrderClass;

  public Purchase: PurchaseClass;

  public MaterialEntrance: MaterialEntranceClass;

  public Transformado: TransformadoClass;

  public UpfrontReturn: UpfrontReturnClass;

  public SavedEmPicking: SavedEmPickingClass;

  public EmailTemplate: EmailTemplateClass;

  public EmailTemplateAttachment: EmailTemplateAttachmentClass;

  public Prison: PrisonClass;

  public Quebra: QuebraClass;

  public Inventario: InventarioClass;

  public ReturnToProvider: ReturnToProviderClass;

  public EmailVerification: EmailVerificationClass;

  public EmailLog: EmailLogClass;

  public DocumentLineNote: DocumentLineNoteClass;

  public SavedProviderProposal: SavedProviderProposalClass;

  public ProductGoogleSheets: ProductGoogleSheetsClass;

  public Task: TaskClass;

  public TaskMessage: TaskMessageClass;

  public RecurrentTasks: RecurrentTasksClass;

  public TaskRead: TaskReadClass;

  public Theme: ThemeClass;

  public Dashboard: DashboardClass;

  public ChatRapidMessage: ChatRapidMessageClass;

  public SideMenu: SideMenuClass;

  public ErrorLog: ErrorLogClass;

  constructor({ processEnvironment, services, gatewayUrl, apikey, tokenBearer }: IOptions) {
    this.processEnvironment = processEnvironment || 'localhost';

    const defaultHeaders = new AxiosHeaders();
    if (apikey) {
      defaultHeaders.set('apikey', apikey);
    }
    if (tokenBearer) {
      defaultHeaders.setAuthorization(tokenBearer);
    }

    this.client = axios.create({
      baseURL: gatewayUrl,
      headers: defaultHeaders,
    });

    const getServicePath = (service: IService) => {
      const baseUrl = services[service];
      const servicePath = processEnvironment === 'localhost' ? '/' : `${service}/`;

      return `${baseUrl}${servicePath}`;
    };

    this.services = {
      stock: getServicePath('stock'),
      users: getServicePath('users'),
      integration: getServicePath('integration'),
      documents: getServicePath('documents'),
      view: getServicePath('view'),
      print: getServicePath('print'),
      tickets: getServicePath('tickets'),
    };

    const getModuleParams = (service: IService, module: string): IModuleConstructor => ({
      api: this.client,
      route: `${this.services[service]}${module}/`,
      publicRoute: `${this.services[service]}${module}/`,
    });

    this.BankData = new BankDataClass(getModuleParams('users', 'bank_data'));

    this.Client = new ClientClass(getModuleParams('users', 'client'));

    this.ClientType = new ClientTypeClass(getModuleParams('users', 'client_type'));

    this.Chat = new ChatClass(getModuleParams('stock', 'chat'));

    this.Commissioner = new CommissionerClass(getModuleParams('users', 'commissioner'));

    this.Country = new CountryClass(getModuleParams('users', 'country'));

    this.DocumentHeader = new DocumentHeaderClass(getModuleParams('stock', 'document_header'));

    this.BillOfLading = new BillOfLadingClass(getModuleParams('stock', 'document_header'));

    this.ProductionOrder = new ProductionOrderClass(getModuleParams('stock', 'document_header'));

    this.Proforma = new ProformaClass(getModuleParams('stock', 'document_header'));

    this.PropostaFornecedor = new PropostaFornecedorClass(
      getModuleParams('stock', 'document_header'),
    );

    this.ProviderCreditNoteFromReturn = new ProviderCreditNoteFromReturnClass(
      getModuleParams('stock', 'document_header'),
    );

    this.ProviderFinancialCreditNote = new ProviderFinancialCreditNoteClass(
      getModuleParams('stock', 'document_header'),
    );

    this.ProviderServiceInvoice = new ProviderServiceInvoiceClass(
      getModuleParams('stock', 'document_header'),
    );

    this.DocumentLine = new DocumentLineClass(getModuleParams('stock', 'document_line'));

    this.DocumentLineAssoc = new DocumentLineAssocClass(
      getModuleParams('stock', 'document_line_assoc'),
    );

    this.DocumentType = new DocumentTypeClass(getModuleParams('stock', 'document_type'));

    this.Favorites = new FavoritesClass(getModuleParams('users', 'favorite'));

    this.File = new FileClass(getModuleParams('stock', 'file'));

    this.GoogleSheets = new GoogleSheetsClass(getModuleParams('integration', 'google'));

    this.GroupPermissions = new GroupPermissionsClass(getModuleParams('users', 'group_permission'));

    this.Language = new LanguageClass(getModuleParams('users', 'language'));

    this.Location = new LocationClass(getModuleParams('stock', 'location'));

    this.Logout = new LogoutClass(getModuleParams('users', 'logout'));

    this.Multimedia = new MultimediaClass(getModuleParams('documents', 'multimedia'));

    this.Observation = new ObservationClass(getModuleParams('integration', 'observation'));

    this.ObservationType = new ObservationTypeClass(
      getModuleParams('integration', 'observation_type'),
    );

    this.Password = new PasswordClass(getModuleParams('users', 'password'));

    this.Permissions = new PermissionsClass(getModuleParams('users', 'permission'));

    this.ProductImage = new ProductImageClass(getModuleParams('stock', 'product_image'));

    this.Promotion = new PromotionClass(getModuleParams('stock', 'promotion'));

    this.PromotionAssoc = new PromotionAssocClass(getModuleParams('stock', 'promotion_assoc'));

    this.Providers = new ProvidersClass(getModuleParams('users', 'provider'));

    this.ProviderType = new ProviderTypeClass(getModuleParams('users', 'provider_type'));

    this.PurchaseConditions = new PurchaseConditionsClass(
      getModuleParams('users', 'purchase_condition'),
    );

    this.ReasonForExemption = new ReasonForExemptionClass(
      getModuleParams('users', 'reason_for_exemption'),
    );

    this.RefreshToken = new RefreshTokenClass(getModuleParams('users', 'refresh-token'));

    this.SegmentsArea = new SegmentsAreaClass(getModuleParams('users', 'segments_area'));

    this.Sessions = new SessionsClass(getModuleParams('users', 'sessions'));

    this.Shippings = new ShippingsClass(getModuleParams('users', 'shipping'));

    this.StoreOperator = new StoreOperatorClass(getModuleParams('users', 'store_operator'));

    this.Swift = new SwiftClass(getModuleParams('users', 'swift'));

    this.TypeOfLocation = new TypeOfLocationClass(getModuleParams('stock', 'type_of_location'));

    this.UnitOfMeasure = new UnitOfMeasureClass(getModuleParams('stock', 'unit_of_measure'));

    this.UserPermissions = new UserPermissionsClass(getModuleParams('users', 'user_permission'));

    this.UserPositions = new UserPositionsClass(getModuleParams('users', 'user_position'));

    this.Users = new UsersClass(getModuleParams('users', 'users'));

    this.VatTax = new VatTaxClass(getModuleParams('stock', 'vat_tax'));

    this.VatTaxZone = new VatTaxZoneClass(getModuleParams('stock', 'vat_tax_zone'));

    this.Workflow = new WorkflowClass(getModuleParams('stock', 'workflow'));

    this.DeliveryMethods = new DeliveryMethodsClass(getModuleParams('users', 'delivery_methods'));

    this.MaturityDates = new MaturityDatesClass(getModuleParams('users', 'maturity_dates'));

    this.PaymentMethods = new PaymentMethodsClass(getModuleParams('users', 'payment_methods'));

    this.Vehicles = new VehiclesClass(getModuleParams('users', 'vehicles'));

    this.ExternalDocumentType = new ExternalDocumentTypeClass(
      getModuleParams('stock', 'external_document_type'),
    );

    this.DocumentSet = new DocumentSetClass(getModuleParams('stock', 'document_set'));

    this.Payment = new PaymentClass(getModuleParams('stock', 'payment'));

    this.ExternalDocumentHeader = new ExternalDocumentHeaderClass(
      getModuleParams('stock', 'external_document_header'),
    );

    this.ClientCurrentAccount = new ClientCurrentAccountClass(
      getModuleParams('stock', 'client_current_account'),
    );

    this.ProviderCurrentAccount = new ProviderCurrentAccountClass(
      getModuleParams('stock', 'provider_current_account'),
    );

    this.VatValidation = new VatValidationClass(getModuleParams('stock', 'vat_validation'));

    this.StockMovement = new StockMovementClass(getModuleParams('stock', 'stock_movement'));

    this.ZipCode = new ZipCodeClass(getModuleParams('users', 'zip_code'));

    this.Tenant = new TenantClass(getModuleParams('users', 'tenant'));

    this.PreSale = new PreSaleClass(getModuleParams('stock', 'pre_sale'));

    this.PreSaleProduct = new PreSaleProductClass(getModuleParams('stock', 'pre_sale_product'));

    this.OrderManagement = new OrderManagementClass(getModuleParams('stock', 'order_management'));

    this.Npc = new NpcClass(getModuleParams('print', 'npc'));

    this.Printer = new PrinterClass(getModuleParams('print', 'printer'));

    this.SchedulePrintJob = new SchedulePrintJobClass(
      getModuleParams('print', 'schedule_print_job'),
    );

    this.QueryList = new QueryListClass(getModuleParams('stock', 'query'));

    this.QueryParameter = new QueryParameterClass(getModuleParams('stock', 'query_parameter'));

    this.ReturnReason = new ReturnReasonClass(getModuleParams('stock', 'return_reason'));

    this.PropostaSheets = new PropostaSheetsClass(getModuleParams('stock', 'proposta_sheets'));

    this.Schedule = new ScheduleClass(getModuleParams('stock', 'schedule'));

    this.GoogleFilePermission = new GoogleFilePermissionClass(
      getModuleParams('integration', 'google_file_permission'),
    );

    this.Settings = new SettingsClass(getModuleParams('integration', 'settings'));

    this.Tickets = new TicketsClass(getModuleParams('tickets', 'tickets'));

    this.Channel = new ChannelClass(getModuleParams('tickets', 'channel'));

    this.TicketsLanguage = new TicketsLanguageClass(getModuleParams('tickets', 'tickets_language'));

    this.Clt = new CltClass(getModuleParams('tickets', 'clt'));

    this.StartDocumentHeaderLastUpdate = new StartDocumentHeaderLastUpdateClass(
      getModuleParams('stock', 'start_document_header_last_update'),
    );

    this.Persona = new PersonaClass(getModuleParams('users', 'persona'));

    this.ProjectInfo = new ProjectInfoClass(getModuleParams('integration', 'project_info'));

    this.Order = new OrderClass(getModuleParams('stock', 'order'));

    this.Purchase = new PurchaseClass(getModuleParams('stock', 'purchase'));

    this.MaterialEntrance = new MaterialEntranceClass(
      getModuleParams('stock', 'material_entrance'),
    );

    this.Transformado = new TransformadoClass(getModuleParams('stock', 'transformado'));

    this.UpfrontReturn = new UpfrontReturnClass(getModuleParams('stock', 'upfront_return'));

    this.SavedEmPicking = new SavedEmPickingClass(getModuleParams('stock', 'saved_em_picking'));

    this.EmailTemplate = new EmailTemplateClass(getModuleParams('integration', 'email_template'));

    this.EmailTemplateAttachment = new EmailTemplateAttachmentClass(
      getModuleParams('integration', 'email_template_attachment'),
    );

    this.Prison = new PrisonClass(getModuleParams('stock', 'prison'));

    this.Quebra = new QuebraClass(getModuleParams('stock', 'quebra'));

    this.Inventario = new InventarioClass(getModuleParams('stock', 'inventario'));

    this.ReturnToProvider = new ReturnToProviderClass(
      getModuleParams('stock', 'return_to_provider'),
    );

    this.EmailVerification = new EmailVerificationClass(
      getModuleParams('integration', 'email_verification'),
    );

    this.EmailLog = new EmailLogClass(getModuleParams('integration', 'email_log'));

    this.DocumentLineNote = new DocumentLineNoteClass(
      getModuleParams('stock', 'document_line_note'),
    );

    this.SavedProviderProposal = new SavedProviderProposalClass(
      getModuleParams('stock', 'saved_provider_proposal'),
    );

    this.ProductGoogleSheets = new ProductGoogleSheetsClass(
      getModuleParams('stock', 'product_google'),
    );

    this.Task = new TaskClass(getModuleParams('stock', 'task'));

    this.TaskMessage = new TaskMessageClass(getModuleParams('stock', 'task_message'));

    this.RecurrentTasks = new RecurrentTasksClass(getModuleParams('stock', 'recurrent_tasks'));

    this.TaskRead = new TaskReadClass(getModuleParams('stock', 'task_read'));

    this.Theme = new ThemeClass(getModuleParams('users', 'theme'));

    this.Dashboard = new DashboardClass(getModuleParams('stock', 'dashboard'));

    this.ChatRapidMessage = new ChatRapidMessageClass(
      getModuleParams('stock', 'chat_rapid_message'),
    );

    this.SideMenu = new SideMenuClass(getModuleParams('stock', 'side_menu'));

    this.ErrorLog = new ErrorLogClass(getModuleParams('view', 'error_log'));
  }
}
