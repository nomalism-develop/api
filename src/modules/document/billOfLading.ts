import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export default class Repository implements Nomalism.BillOfLading.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async create(
    body: Nomalism.BillOfLading.ICreateRequest,
  ): Promise<Nomalism.shared.ITaskCluster[]> {
    const response = await this.api.post(`${this.route}bill_of_lading`, body);
    return response.data;
  }
}
