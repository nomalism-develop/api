import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import ICreateInventoryRequest = Nomalism.Inventario.ICreateInventoryRequest;

export default class Repository implements Nomalism.Inventario.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async createInventory(data: ICreateInventoryRequest): Promise<void> {
    await this.api.post(`${this.route}`, data);
  }
}
