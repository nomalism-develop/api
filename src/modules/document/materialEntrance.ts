import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import IUnfinishedPickingLineGroup = Nomalism.MaterialEntrance.IUnfinishedPickingLineGroup;
export import IUnfinishedPickingsResponse = Nomalism.MaterialEntrance.IUnfinishedPickingsResponse;

export import IPrintLabelParamsRequest = Nomalism.MaterialEntrance.IPrintLabelParamsRequest;
export import IPrintLabelQueryRequest = Nomalism.MaterialEntrance.IPrintLabelQueryRequest;

export import IPrintBulkLabelRequest = Nomalism.MaterialEntrance.IPrintBulkLabelRequest;

export import IUnfinishedPickingLineGroupExtended = Nomalism.MaterialEntrance.IUnfinishedPickingLineGroupExtended;

export import IGetMaterialEntranceInfoStock = Nomalism.MaterialEntrance.IGetMaterialEntranceInfoStock;

export import IGetMaterialEntranceInfoClientOrder = Nomalism.MaterialEntrance.IGetMaterialEntranceInfoClientOrder;

export import IGetMaterialEntranceInfoResponse = Nomalism.MaterialEntrance.IGetMaterialEntranceInfoResponse;

export import IUndoEntradaDeMaterialRequest = Nomalism.MaterialEntrance.IUndoEntradaDeMaterialRequest;

export import ICreateMaterialEntranceClientOrderOptionEnum = Nomalism.MaterialEntrance.ICreateMaterialEntranceClientOrderOptionEnum;

export import ICreateMaterialEntranceClientOrderOption = Nomalism.MaterialEntrance.ICreateMaterialEntranceClientOrderOption;

export import createMaterialEntranceClientOrderOptions = Nomalism.MaterialEntrance.createMaterialEntranceClientOrderOptions;

export import ICreateMaterialEntranceStockOptionEnum = Nomalism.MaterialEntrance.ICreateMaterialEntranceStockOptionEnum;

export import ICreateMaterialEntranceStockOption = Nomalism.MaterialEntrance.ICreateMaterialEntranceStockOption;

export import createMaterialEntranceStockOptions = Nomalism.MaterialEntrance.createMaterialEntranceStockOptions;

export import ICreateMaterialEntranceStockWithoutOrderOptionEnum = Nomalism.MaterialEntrance.ICreateMaterialEntranceStockWithoutOrderOptionEnum;

export import ICreateMaterialEntranceStockWithoutOrderOption = Nomalism.MaterialEntrance.ICreateMaterialEntranceStockWithoutOrderOption;

export import createMaterialEntranceStockWithoutOrderOptions = Nomalism.MaterialEntrance.createMaterialEntranceStockWithoutOrderOptions;

export import IEntradaDeMaterialClientOrder = Nomalism.MaterialEntrance.IEntradaDeMaterialClientOrder;

export import IEntradaDeMaterialStock = Nomalism.MaterialEntrance.IEntradaDeMaterialStock;

export import IEntradaDeMaterialNC = Nomalism.MaterialEntrance.IEntradaDeMaterialNC;

export import IFinalizarEntradaDeMaterialRequest = Nomalism.MaterialEntrance.IFinalizarEntradaDeMaterialRequest;

export import IFinalizarEntradaDeMaterialResponse = Nomalism.MaterialEntrance.IFinalizarEntradaDeMaterialResponse;

export default class Repository implements Nomalism.MaterialEntrance.IApi {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async findMaterialEntranceProviders(): Promise<Nomalism.shared.IFindMinifiedResponse[]> {
    const response = await this.api.get(`${this.route}material_entrance_providers`);
    return response.data;
  }

  async findUnfinishedPickings({
    id,
  }: Nomalism.shared.IFindByIdRequest): Promise<IUnfinishedPickingsResponse[]> {
    const response = await this.api.get(`${this.route}unfinished_pickings/${id}`);
    return response.data;
  }

  async finalizarEntradaMaterial(
    data: IFinalizarEntradaDeMaterialRequest,
  ): Promise<IFinalizarEntradaDeMaterialResponse> {
    const response = await this.api.post(`${this.route}finalize_material_entrance`, data);
    return response.data;
  }

  async undoEntradaMaterial(data: IUndoEntradaDeMaterialRequest): Promise<void> {
    await this.api.post(`${this.route}undo_material_entrance`, data);
  }

  printLabelToPdfUrl(
    { product_id }: IPrintLabelParamsRequest,
    { quantity, ef_name, pc_document_number, prison, note }: IPrintLabelQueryRequest,
    token: string,
  ): string {
    const qs = new URLSearchParams();
    qs.set('ef_name', ef_name);
    qs.set('quantity', quantity.toString());
    if (pc_document_number) {
      qs.set('pc_document_number', pc_document_number.toString());
    }
    if (note) {
      qs.set('note', note.toString());
    }
    qs.set('prison', prison.toString());
    qs.set('token', token);
    return `${this.publicRoute}print_label_pdf/${product_id}?${qs.toString()}`;
  }

  printBulkLabelToPdfUrl({
    groupLabel,
    saved_em_picking_ids,
    token,
  }: IPrintBulkLabelRequest): string {
    const qs = new URLSearchParams();
    qs.set('groupLabel', groupLabel.toString());
    qs.set('saved_em_picking_ids', saved_em_picking_ids);
    qs.set('token', token);
    return `${this.publicRoute}print_bulk_label_pdf?${qs.toString()}`;
  }
}
