import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import IUndoProviderOrderRequest = Nomalism.Order.IUndoProviderOrderRequest;

export import IConferirEncomendaClienteOptions = Nomalism.Order.IConferirEncomendaClienteOptions;
export import IConferirEncomendaClienteRequest = Nomalism.Order.IConferirEncomendaClienteRequest;
export import IConferirEncomendaClienteResponse = Nomalism.Order.IConferirEncomendaClienteResponse;

export import IFindStockToOrderResponse = Nomalism.Order.IFindStockToOrderResponse;
export import IFindStockToOrderRequest = Nomalism.Order.IFindStockToOrderRequest;

export import ISetProviderRequest = Nomalism.Order.ISetProviderRequest;

export import IUnsetCativado = Nomalism.Order.IUnsetCativado;

export import IFindRequest = Nomalism.Order.IFindRequest;
export import IOrderType = Nomalism.Order.IOrderType;
export import IFindResponse = Nomalism.Order.IFindResponse;

export const { ConferirEncomendaClienteOptionsEnum, conferirEncomendaClienteOptions } =
  Nomalism.Order;

export default class Repository implements Nomalism.Order.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async unsetCativado(params: IUnsetCativado): Promise<void> {
    const response = await this.api.post(`${this.route}unset_cativado`, {
      params,
    });
    return response.data;
  }

  async findStockToOrder(params: IFindStockToOrderRequest): Promise<IFindStockToOrderResponse[]> {
    const response = await this.api.get(`${this.route}check_lines_to_order`, {
      params,
    });
    return response.data;
  }

  async findProvidersWithStockToOrder(): Promise<Nomalism.shared.IFindMinifiedResponse[]> {
    const response = await this.api.get(`${this.route}stock_order_provider`);
    return response.data;
  }

  async undoEncomendaVA(selector: Nomalism.shared.IFindByIdRequest): Promise<void> {
    await this.api.delete(`${this.route}${selector.id}/undo_encomenda_va`);
  }

  async setProvider(data: ISetProviderRequest): Promise<void> {
    await this.api.post(`${this.route}set_provider`, data);
  }

  async unsetProvider({ id }: Nomalism.shared.IFindByIdRequest): Promise<void> {
    await this.api.post(`${this.route}${id}/unset_provider`);
  }

  async undoEncomendaStock({ id }: Nomalism.shared.IFindByIdRequest): Promise<void> {
    await this.api.post(`${this.route}${id}/undo_encomenda_stock`);
  }

  async undoProviderOrder(data: IUndoProviderOrderRequest): Promise<void> {
    await this.api.post(`${this.route}undo_provider_order`, data);
  }

  async conferirEncomendaCliente(
    data: IConferirEncomendaClienteRequest,
  ): Promise<IConferirEncomendaClienteResponse[]> {
    const response = await this.api.post(`${this.route}check_client_order`, data);
    return response.data;
  }

  async find(params: IFindRequest): Promise<IFindResponse[]> {
    const response = await this.api.get(`${this.route}`, { params });
    return response.data;
  }
}
