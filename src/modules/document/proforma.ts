import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export default class Repository implements Nomalism.Proforma.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async create(data: Nomalism.Proforma.ICreateRequest): Promise<Nomalism.shared.ITaskCluster[]> {
    const result = await this.api.post(`${this.route}pro_forma`, data);
    return result.data;
  }
}
