import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export default class Repository implements Nomalism.PropostaFornecedor.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async createProviderProposal(
    data: Nomalism.PropostaFornecedor.IProviderProposalRequest,
  ): Promise<string> {
    const result = await this.api.post(`${this.route}provider_proposal`, data);
    return result.data;
  }
}
