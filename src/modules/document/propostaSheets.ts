import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import IConflictEnum = Nomalism.PropostaSheets.IConflictEnum;

export import IConflict = Nomalism.PropostaSheets.IConflict;

export import IFindByGooglesheetIdResponse = Nomalism.PropostaSheets.IFindByGooglesheetIdResponse;

export import IDocumentLinePendingConflict = Nomalism.PropostaSheets.Entity;

export import IConflictFindByOwnerIdResponse = Nomalism.PropostaSheets.IConflictFindByOwnerIdResponse;

export import ICreateSheetFromDocumentResponse = Nomalism.PropostaSheets.ICreateSheetFromDocumentResponse;

export import ICloneSheetToNewDocumentRequest = Nomalism.PropostaSheets.ICloneSheetToNewDocumentRequest;
export import ICloneSheetToNewDocumentResponse = Nomalism.PropostaSheets.ICloneSheetToNewDocumentResponse;

export import IIsProcessingResponse = Nomalism.PropostaSheets.IIsProcessingResponse;

export import IImportFromSheetRequest = Nomalism.PropostaSheets.IImportFromSheetRequest;

export default class Repository implements Nomalism.PropostaSheets.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async isProcessing(params: Nomalism.shared.IFindByIdRequest): Promise<IIsProcessingResponse> {
    const response = await this.api.get(`${this.route}is_processing`, {
      params,
    });

    return response.data;
  }

  async importFromSheet(params: IImportFromSheetRequest): Promise<IFindByGooglesheetIdResponse> {
    const response = await this.api.post(`${this.route}import_from_sheet`, {
      params,
    });

    return response.data;
  }

  async createSheetFromDocument(
    params: Nomalism.shared.IFindByIdRequest,
  ): Promise<ICreateSheetFromDocumentResponse> {
    const response = await this.api.post(`${this.route}create_sheet_from_document`, params);

    return response.data;
  }

  async cloneSheetsToNewDocument(
    params: ICloneSheetToNewDocumentRequest,
  ): Promise<ICloneSheetToNewDocumentResponse> {
    const response = await this.api.post(`${this.route}clone_sheet_to_new_document`, params);

    return response.data;
  }

  async conflictAccept(params: Nomalism.shared.IFindByIdRequest): Promise<void> {
    await this.api.post(`${this.route}conflict_accept`, params);
  }

  async conflictDelete(data: Nomalism.shared.IFindByIdRequest): Promise<void> {
    await this.api.delete(`${this.route}conflict_delete`, {
      data,
    });
  }

  async conflictFindByOwnerId(
    params: Nomalism.shared.IFindByOwnerIdRequest,
  ): Promise<IConflictFindByOwnerIdResponse[]> {
    const response = await this.api.get(`${this.route}conflict_find_by_owner_id`, {
      params,
    });

    return response.data;
  }
}
