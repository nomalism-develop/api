import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export default class Repository implements Nomalism.ProviderCreditNoteFromReturn.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async findProviderReturn(
    params: Nomalism.ProviderCreditNoteFromReturn.IFindProviderReturnRequest,
  ): Promise<Nomalism.ProviderCreditNoteFromReturn.IFindProviderReturnResponse[]> {
    const response = await this.api.get(`${this.route}provider_return`, {
      params,
    });
    return response.data;
  }

  async createProviderCreditNoteFromReturn(
    data: Nomalism.ProviderCreditNoteFromReturn.ICreateProviderCreditNoteFromReturnRequest,
  ): Promise<void> {
    await this.api.post(`${this.route}provider_credit_note_from_return`, data);
  }
}
