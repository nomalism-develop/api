import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export default class Repository implements Nomalism.ProviderFinancialCreditNote.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async findProviderInvoice(
    params: Nomalism.ProviderFinancialCreditNote.IFindProviderInvoiceRequest,
  ): Promise<Nomalism.ProviderFinancialCreditNote.IFindProviderInvoiceResponse[]> {
    const response = await this.api.get(`${this.route}provider_invoice`, {
      params,
    });
    return response.data;
  }

  async createProviderFinancialCreditNote(
    data: Nomalism.ProviderFinancialCreditNote.ICreateProviderFinancialCreditNoteRequest,
  ): Promise<void> {
    await this.api.post(`${this.route}provider_financial_credit_note`, data);
  }
}
