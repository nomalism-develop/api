import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export default class Repository implements Nomalism.ProviderServiceInvoice.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async createProviderServiceInvoice(
    data: Nomalism.ProviderServiceInvoice.ICreateProviderServiceInvoiceRequest,
  ): Promise<void> {
    await this.api.post(`${this.route}provider_service_invoice`, data);
  }
}
