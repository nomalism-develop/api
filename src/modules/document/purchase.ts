import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import IUnpaidMaterialEntranceLineType = Nomalism.Purchase.IUnpaidMaterialEntranceLineType;

export import IUnpaidMaterialEntranceLineResponse = Nomalism.Purchase.IUnpaidMaterialEntranceLineResponse;

export import IUnpaidMaterialEntranceItemResponse = Nomalism.Purchase.IUnpaidMaterialEntranceItemResponse;
export import IPurchaseFromProviderRequest = Nomalism.Purchase.IPurchaseFromProviderRequest;
export import IUnpaidMaterialEntranceResponse = Nomalism.Purchase.IUnpaidMaterialEntranceResponse;

export default class Repository implements Nomalism.Purchase.IApi {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async purchaseFromProvider(data: IPurchaseFromProviderRequest): Promise<void> {
    await this.api.post(`${this.route}purchase_from_provider`, data);
  }

  async findUnpaidMaterialEntrance(
    params: Nomalism.shared.IFindByIdRequest,
  ): Promise<IUnpaidMaterialEntranceResponse> {
    const response = await this.api.get(`${this.route}unpaid_material_entrance`, {
      params,
    });
    return response.data;
  }

  async findUnpaidMaterialEntranceProviders(): Promise<Nomalism.shared.IFindMinifiedResponse[]> {
    const response = await this.api.get(`${this.route}unpaid_material_entrance_providers`);
    return response.data;
  }

  async sync(data: Nomalism.Purchase.ISyncRequest): Promise<Nomalism.Purchase.ISyncResponse> {
    const response = await this.api.post(`${this.route}sync`, data);
    return response.data;
  }
}
