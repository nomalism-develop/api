import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import IInventoryLossResponse = Nomalism.Quebra.IInventoryLossResponse;

export import ICreateManyRequest = Nomalism.Quebra.ICreateManyRequest;

export import IDeleteInventoryLossRequest = Nomalism.Quebra.IDeleteInventoryLossRequest;

export default class Repository implements Nomalism.Quebra.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async findInventoryLosses(): Promise<IInventoryLossResponse[]> {
    const response = await this.api.get(`${this.route}`);
    return response.data;
  }

  async findInventoryLossesProviders(): Promise<Nomalism.shared.IFindMinifiedResponse[]> {
    const response = await this.api.get(`${this.route}providers`);
    return response.data;
  }

  async create(body: ICreateManyRequest): Promise<string> {
    const response = await this.api.post(`${this.route}`, body);
    return response.data;
  }

  async createMany(body: ICreateManyRequest): Promise<string> {
    const response = await this.api.post(`${this.route}create_many`, body);
    return response.data;
  }

  async deleteInventoryLosses(data: IDeleteInventoryLossRequest): Promise<void> {
    await this.api.delete(`${this.route}inventory_loss`, { data });
  }
}
