import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import IFindOptionalByOwnerIdRequest = Nomalism.shared.IFindOptionalByOwnerIdRequest;
export import IFindLinesToReturnResponse = Nomalism.ReturnToProvider.IFindLinesToReturnResponse;

export import ICheckLinesToReturnOptions = Nomalism.ReturnToProvider.ICheckLinesToReturnOptions;

export import ICheckLinesToReturnLinesRequest = Nomalism.ReturnToProvider.ICheckLinesToReturnLinesRequest;
export import ICheckLinesToReturnRequest = Nomalism.ReturnToProvider.ICheckLinesToReturnRequest;

export default class Repository implements Nomalism.ReturnToProvider.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async findLinesToReturn(
    params: IFindOptionalByOwnerIdRequest,
  ): Promise<IFindLinesToReturnResponse[]> {
    const response = await this.api.get(`${this.route}lines_to_return`, {
      params,
    });
    return response.data;
  }

  async findLinesToReturnProviders(): Promise<Nomalism.shared.IFindMinifiedResponse[]> {
    const response = await this.api.get(`${this.route}lines_to_return_providers`);
    return response.data;
  }

  async checkLinesToReturn(data: ICheckLinesToReturnRequest): Promise<void> {
    await this.api.post(`${this.route}check_to_return`, data);
  }
}
