import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import ICreateTransformado = Nomalism.Transformado.ICreateTransformado;

export import IProductsCreateTransformado = Nomalism.Transformado.IProductsCreateTransformado;

export default class Repository implements Nomalism.Transformado.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async create(body: ICreateTransformado): Promise<string> {
    const response = await this.api.post(`${this.route}`, body);
    return response.data;
  }
}
