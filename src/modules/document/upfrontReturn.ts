import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export type ICreateRequest = Nomalism.UpfrontReturn.ICreateRequest;

export default class Repository implements Nomalism.UpfrontReturn.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async create(selector: Nomalism.shared.IFindByIdRequest, data: ICreateRequest): Promise<void> {
    const response = await this.api.post(`${this.route}${selector.id}`, data);
    return response.data;
  }
}
