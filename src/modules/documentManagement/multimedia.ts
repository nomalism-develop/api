import { AxiosRequestHeaders, AxiosInstance } from 'axios';
import Nomalism from '@nomalism-com/types';
import { IModuleConstructor } from '../../main';

export import IEntity = Nomalism.Multimedia.Entity;

export default class Repository implements Nomalism.Multimedia.IApi {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  getUrl(multimediaId: string, download = false) {
    const qs = new URLSearchParams();
    if (download) qs.set('download', download.toString());
    return `${this.publicRoute}${multimediaId}?${qs.toString()}`;
  }

  async create(
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    multipartFormData: any,
    headers?: AxiosRequestHeaders,
  ): Promise<Nomalism.Multimedia.Entity> {
    const response = await this.api.post(`${this.publicRoute}upload`, multipartFormData, {
      headers: {
        ...(headers || {}),
        'content-type': 'multipart/form-data',
      },
      maxBodyLength: Infinity,
      maxContentLength: Infinity,
    });
    return response.data;
  }
}
