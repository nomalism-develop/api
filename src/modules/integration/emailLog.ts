import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import IEmailAttachment = Nomalism.EmailLog.IEmailAttachment;
export import IEmailAddress = Nomalism.EmailLog.IEmailAddress;
export import IFindByIdResponse = Nomalism.EmailLog.IFindByIdResponse;

export import IFindRequest = Nomalism.EmailLog.IFindRequest;
export import IFindResponse = Nomalism.EmailLog.IFindResponse;

export default class Repository implements Nomalism.EmailLog.IApi {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async find(params: IFindRequest): Promise<IFindResponse[]> {
    const response = await this.api.get(`${this.route}`, { params });
    return response.data;
  }

  async findById({ id }: Nomalism.shared.IFindByIdRequest): Promise<IFindByIdResponse> {
    const response = await this.api.get(`${this.route}${id}`);
    return response.data;
  }

  async findByDocumentHeaderId({
    id,
  }: Nomalism.shared.IFindByIdRequest): Promise<IFindByIdResponse[]> {
    const response = await this.api.get(`${this.route}document_header/${id}`);
    return response.data;
  }

  async checkSent(body: Nomalism.EmailLog.ICheckSent): Promise<boolean[]> {
    const response = await this.api.post(`${this.route}check_sent`, body);
    return response.data;
  }
}
