import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import IEntity = Nomalism.EmailTemplate.Entity;
export import IEntityExtended = Nomalism.EmailTemplate.IEntityExtended;

export import ICreateRequest = Nomalism.EmailTemplate.ICreateRequest;

export import IUpdateRequest = Nomalism.EmailTemplate.IUpdateRequest;

export import ISendRequest = Nomalism.EmailTemplate.ISendRequest;

export default class Repository implements Nomalism.EmailTemplate.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async find(): Promise<Nomalism.EmailTemplate.IEntityExtended[]> {
    const response = await this.api.get(`${this.route}`);
    return response.data;
  }

  async create(data: ICreateRequest): Promise<string> {
    const response = await this.api.post(`${this.route}`, data);
    return response.data;
  }

  async update(selector: Nomalism.shared.IFindByIdRequest, data: IUpdateRequest): Promise<void> {
    const response = await this.api.put(`${this.route}${selector.id}`, data);
    return response.data;
  }

  async deleteOne(selector: Nomalism.shared.IFindByIdRequest): Promise<void> {
    await this.api.delete(`${this.route}${selector.id}`);
  }

  async send(data: ISendRequest): Promise<void> {
    const response = await this.api.post(`${this.route}send`, data);
    return response.data;
  }
}
