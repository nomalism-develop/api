import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import IEntity = Nomalism.EmailTemplateAttachment.Entity;

export import ICreateRequest = Nomalism.EmailTemplateAttachment.ICreateRequest;

export default class Repository implements Nomalism.EmailTemplateAttachment.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async create(data: ICreateRequest): Promise<string> {
    const response = await this.api.post(`${this.route}`, data);
    return response.data;
  }

  async deleteOne(selector: Nomalism.shared.IFindByIdRequest): Promise<void> {
    await this.api.delete(`${this.route}${selector.id}`);
  }
}
