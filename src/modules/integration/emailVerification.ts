import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export default class Repository implements Nomalism.EmailVerification.IApi {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async isVerified(params: Nomalism.EmailVerification.IIsVerifiedRequest): Promise<boolean> {
    const response = await this.api.get(`${this.route}`, { params });
    return response.data;
  }

  async sendVerificationEmail(
    data: Nomalism.EmailVerification.ISendVerificationRequest,
  ): Promise<void> {
    const response = await this.api.post(`${this.route}`, data);
    return response.data;
  }

  async markAsVerified(data: Nomalism.EmailVerification.IMarkAsVerifiedRequest): Promise<void> {
    const response = await this.api.put(`${this.publicRoute}`, data);
    return response.data;
  }
}
