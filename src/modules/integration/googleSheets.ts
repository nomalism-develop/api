import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import ICloneRequest = Nomalism.GoogleSheets.ICloneRequest;
export import ICloneResponse = Nomalism.GoogleSheets.ICloneResponse;
export import IUpdateRequest = Nomalism.GoogleSheets.IUpdateRequest;
export import IGetDataResponse = Nomalism.GoogleSheets.IGetDataResponse;

export import IGetUserData = Nomalism.GoogleSheets.IGetUserData;

export import IGetProduct = Nomalism.GoogleSheets.IGetProduct;

export import IGetProductGroup = Nomalism.GoogleSheets.IGetProductGroup;

export import IGetProductType = Nomalism.GoogleSheets.IGetProductType;
export import IExportRequest = Nomalism.GoogleSheets.IExportRequest;

export default class Repository implements Nomalism.GoogleSheets.IController {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async clone(
    selector: Nomalism.shared.IFindByIdRequest,
    params?: ICloneRequest,
  ): Promise<ICloneResponse> {
    const response = await this.api.get(`${this.route}clone/${selector.id}`, {
      params,
    });

    return response.data;
  }

  async export(params: IExportRequest): Promise<string> {
    const response = await this.api.get(`${this.route}export`, { params });
    return response.data;
  }

  async getData(selector: Nomalism.shared.IFindByIdRequest): Promise<IGetDataResponse> {
    const response = await this.api.get(`${this.route}${selector.id}`);
    return response.data;
  }

  async update(selector: Nomalism.shared.IFindByIdRequest, data: IUpdateRequest): Promise<void> {
    await this.api.put(`${this.route}${selector.id}`, data);
  }
}
