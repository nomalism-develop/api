import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import IFindByOwnerRequest = Nomalism.Observation.IFindByOwnerRequest;

export import IFindByOwnersRequest = Nomalism.Observation.IFindByOwnersRequest;
export import IFindByOwnerResponse = Nomalism.Observation.IFindByOwnerResponse;

export import ICreateRequest = Nomalism.Observation.ICreateRequest;
export import IEntity = Nomalism.Observation.Entity;

export import IUpdateRequest = Nomalism.Observation.IUpdateRequest;

export default class Repository implements Nomalism.Observation.IApi {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async findByOwnerId(params: IFindByOwnerRequest): Promise<IFindByOwnerResponse[]> {
    const response = await this.api.get(`${this.route}`, {
      params,
    });
    return response.data;
  }

  async findByOwnerIds(body: IFindByOwnersRequest): Promise<IFindByOwnerResponse[]> {
    const response = await this.api.post(`${this.route}by_owners`, body);
    return response.data;
  }

  async create(body: ICreateRequest): Promise<IFindByOwnerResponse> {
    const response = await this.api.post(`${this.route}`, body);
    return response.data;
  }

  async update(
    selector: Nomalism.shared.IFindByIdRequest,
    body: IUpdateRequest,
  ): Promise<IFindByOwnerResponse | null> {
    const response = await this.api.put(`${this.route}${selector.id}`, body);
    return response.data;
  }

  async deleteOne(selector: Nomalism.shared.IFindByIdRequest): Promise<IEntity | null> {
    const response = await this.api.delete(`${this.route}${selector.id}`);
    return response.data;
  }
}
