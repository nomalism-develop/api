import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import IFindMinifiedRequest = Nomalism.shared.IFindMinifiedRequest;
export import IFindMinifiedResponse = Nomalism.shared.IFindMinifiedResponse;

export import ICreateRequest = Nomalism.ObservationType.ICreateRequest;
export import IEntity = Nomalism.ObservationType.Entity;

export import IUpdateRequest = Nomalism.ObservationType.IUpdateRequest;

export default class Repository implements Nomalism.ObservationType.IApi {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async find(): Promise<IEntity[]> {
    const response = await this.api.get(`${this.route}`);
    return response.data;
  }

  async findMinified(params?: IFindMinifiedRequest): Promise<IFindMinifiedResponse[]> {
    const response = await this.api.get(`${this.route}minified`, {
      params,
    });
    return response.data;
  }

  async create(body: ICreateRequest): Promise<IEntity> {
    const response = await this.api.post(`${this.route}`, body);
    return response.data;
  }

  async update(
    selector: Nomalism.shared.IFindByIdRequest,
    body: IUpdateRequest,
  ): Promise<IEntity | null> {
    const response = await this.api.put(`${this.route}${selector.id}`, body);
    return response.data;
  }

  async deleteOne(selector: Nomalism.shared.IFindByIdRequest): Promise<IEntity | null> {
    const response = await this.api.delete(`${this.route}${selector.id}`);
    return response.data;
  }
}
