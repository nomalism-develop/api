import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import ISendEmail = Nomalism.ProjectInfo.ISendEmail;

export default class Repository implements Nomalism.ProjectInfo.IController {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async sendEmail(body: ISendEmail): Promise<void> {
    await this.api.post(`${this.route}sendEmail`, body);
  }
}
