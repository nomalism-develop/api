import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import IEntity = Nomalism.Settings.Entity;
export import ICreateRequest = Nomalism.Settings.ICreateRequest;
export import IUpdateRequest = Nomalism.Settings.IUpdateRequest;
export import IFindByKeyRequest = Nomalism.Settings.IFindByKeyRequest;

export default class Repository implements Nomalism.Settings.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async create(body: ICreateRequest): Promise<IEntity> {
    const response = await this.api.post(`${this.route}`, body);
    return response.data;
  }

  async findByKey(params: IFindByKeyRequest): Promise<IEntity | null> {
    const response = await this.api.get(`${this.publicRoute}by_key`, {
      params,
    });
    return response.data;
  }

  async update(body: IUpdateRequest): Promise<IEntity | null> {
    const response = await this.api.put(`${this.route}`, body);
    return response.data;
  }

  async deleteOne(selector: Nomalism.shared.IFindByIdNumberRequest): Promise<IEntity | null> {
    const response = await this.api.delete(`${this.route}${selector.id}`);
    return response.data;
  }

  async find(): Promise<IEntity[]> {
    const response = await this.api.get(`${this.route}`);
    return response.data;
  }
}
