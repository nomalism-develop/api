import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import IEntity = Nomalism.Printer.Entity;
export import IUpdateRequest = Nomalism.Printer.IUpdateRequest;

export type IFindDetailedResponse = Nomalism.Printer.IFindDetailedResponse;

export default class Repository implements Nomalism.Printer.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async find(): Promise<IFindDetailedResponse[]> {
    const response = await this.api.get(`${this.route}`);
    return response.data;
  }

  async findByOwnerId(params: Nomalism.shared.IFindByOwnerIdRequest): Promise<IEntity[]> {
    const response = await this.api.get(`${this.route}by_owner`, { params });
    return response.data;
  }

  async update(
    selector: Nomalism.shared.IFindByIdRequest,
    body: IUpdateRequest,
  ): Promise<IEntity | null> {
    const response = await this.api.put(`${this.route}${selector.id}`, body);
    return response.data;
  }
}
