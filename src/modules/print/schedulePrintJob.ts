import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export default class Repository implements Nomalism.SchedulePrintJob.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async create(body: Nomalism.SchedulePrintJob.ICreateSchedulePrintJobRequest): Promise<void> {
    await this.api.post(`${this.route}`, body);
  }

  async createMany(
    body: Nomalism.SchedulePrintJob.ICreateManySchedulePrintJobRequest,
  ): Promise<void> {
    await this.api.post(`${this.route}many`, body);
  }

  async deleteOne(selector: Nomalism.shared.IFindByIdRequest): Promise<void> {
    await this.api.delete(`${this.route}${selector.id}`);
  }

  async dispatchSchedulePrintJob(
    body: Nomalism.SchedulePrintJob.IDispatchSchedulePrintJobRequest,
  ): Promise<Nomalism.SchedulePrintJob.IDispatchSchedulePrintJobResponse> {
    const response = await this.api.post(`${this.route}dispatch_schedule_print_job`, body);
    return response.data;
  }

  async findByOwner(
    params: Nomalism.shared.IFindByOwnerIdRequest,
  ): Promise<Nomalism.SchedulePrintJob.Entity[]> {
    const response = await this.api.get(`${this.route}by_owner`, { params });
    return response.data;
  }
}
