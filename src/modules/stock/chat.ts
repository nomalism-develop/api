import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export default class Repository implements Nomalism.Chat.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async findActiveByOwnerId(
    params: Nomalism.shared.IFindByOwnerIdRequest,
  ): Promise<Nomalism.Chat.IPublicFindActiveByOwnerIdResponse> {
    const response = await this.api.get(`${this.route}active`, {
      params,
    });
    return response.data;
  }

  async create(body: Nomalism.Chat.ICreateRequest): Promise<Nomalism.Chat.Entity> {
    const response = await this.api.post(`${this.route}`, body);
    return response.data;
  }

  async update(
    selector: Nomalism.shared.IFindByIdRequest,
    body: Nomalism.Chat.IUpdateRequest,
  ): Promise<void> {
    await this.api.put(`${this.route}${selector.id}`, body);
  }

  async deleteOne(selector: Nomalism.shared.IFindByIdRequest): Promise<void> {
    await this.api.delete(`${this.route}${selector.id}`);
  }

  async resendLast({ owner_id }: Nomalism.shared.IFindByOwnerIdRequest): Promise<void> {
    await this.api.post(`${this.route}resend_last/${owner_id}`);
  }

  async markAllAsRead(selector: Nomalism.shared.IFindByOwnerIdRequest): Promise<void> {
    await this.api.put(`${this.route}mark_all_as_read/${selector.owner_id}`);
  }

  async markAllAsUnread(selector: Nomalism.shared.IFindByOwnerIdRequest): Promise<void> {
    await this.api.put(`${this.route}mark_all_as_unread/${selector.owner_id}`);
  }
}
