import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export default class Repository implements Nomalism.ChatRapidMessage.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async create(
    data: Nomalism.ChatRapidMessage.ICreateRequest,
  ): Promise<Nomalism.ChatRapidMessage.Entity> {
    const response = await this.api.post(`${this.route}`, data);
    return response.data;
  }

  async findMinified(): Promise<Nomalism.ChatRapidMessage.IFindMinifiedResponse[]> {
    const response = await this.api.get(`${this.route}`);
    return response.data;
  }

  async findById(
    selector: Nomalism.shared.IFindByIdRequest,
  ): Promise<Nomalism.ChatRapidMessage.Entity> {
    const response = await this.api.get(`${this.route}${selector.id}`);
    return response.data;
  }

  async update(
    selector: Nomalism.shared.IFindByIdRequest,
    data: Nomalism.ChatRapidMessage.IUpdateRequest,
  ): Promise<void> {
    const response = await this.api.put(`${this.route}${selector.date}`, data);
    return response.data;
  }

  async delete(selector: Nomalism.shared.IFindByIdRequest): Promise<void> {
    await this.api.delete(`${this.route}${selector.id}`);
  }
}
