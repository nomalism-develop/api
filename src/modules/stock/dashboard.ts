import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export default class Repository implements Nomalism.Dashboard.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async findByDate(
    selector: Nomalism.Dashboard.IFindDashboardByDate,
  ): Promise<Nomalism.Dashboard.IDashboardResponse> {
    const response = await this.api.get(`${this.route}${selector.date}`);
    return response.data;
  }
}
