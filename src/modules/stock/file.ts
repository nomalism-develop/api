import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import IFindByOwnerIdRequest = Nomalism.File.IFindByOwnerRequest;
export import IEntity = Nomalism.File.Entity;
export import ICreateRequest = Nomalism.File.ICreateRequest;

export default class Repository implements Nomalism.File.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async findByOwnerId(params: IFindByOwnerIdRequest): Promise<IEntity[]> {
    const response = await this.api.get(`${this.route}by_owner`, {
      params,
    });
    return response.data;
  }

  async create(body: ICreateRequest): Promise<IEntity> {
    const response = await this.api.post(`${this.publicRoute}`, body);
    return response.data;
  }

  async update(
    { id }: Nomalism.shared.IFindByIdRequest,
    body: Nomalism.File.IUpdateRequest,
  ): Promise<void> {
    await this.api.post(`${this.publicRoute}${id}`, body);
  }

  async deleteOne(selector: Nomalism.shared.IFindByIdRequest): Promise<IEntity | null> {
    const response = await this.api.delete(`${this.route}${selector.id}`);
    return response.data;
  }
}
