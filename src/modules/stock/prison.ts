import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import IFindByOwnerIdRequest = Nomalism.shared.IFindByOwnerIdRequest;

export import IFindByIdRequest = Nomalism.shared.IFindByIdRequest;

export import IFindLinesInPrisonResponse = Nomalism.Prison.IFindLinesInPrisonResponse;

export import ICheckLineInPrisonOptions = Nomalism.Prison.ICheckLineInPrisonOptions;

export import ICheckLinesInPrisonLinesRequest = Nomalism.Prison.ICheckLinesInPrisonLinesRequest;
export import ICheckLinesInPrisonRequest = Nomalism.Prison.ICheckLinesInPrisonRequest;

export import ICreateLinesInPrisonRequest = Nomalism.Prison.ICreateLinesInPrisonRequest;

export const { checkLineInPrisonOptions } = Nomalism.Prison;

export default class Repository implements Nomalism.Prison.IApi {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async putLineInPrison(data: IFindByIdRequest): Promise<void> {
    const response = await this.api.post(`${this.route}put_in_prison`, data);
    return response.data;
  }

  async removeLineFromPrison(data: IFindByIdRequest): Promise<void> {
    const response = await this.api.post(`${this.route}remove_from_prison`, data);
    return response.data;
  }

  async findLinesInPrison(): Promise<IFindLinesInPrisonResponse[]> {
    const response = await this.api.get(`${this.route}in_prison`, {});
    return response.data;
  }

  async findLinesInPrisonProviders(): Promise<Nomalism.shared.IFindMinifiedResponse[]> {
    const response = await this.api.get(`${this.route}in_prison_providers`, {});
    return response.data;
  }

  async checkLinesInPrison(data: ICheckLinesInPrisonRequest): Promise<void> {
    await this.api.post(`${this.route}check_in_prison`, data);
  }

  async createLinesInPrison(data: ICreateLinesInPrisonRequest): Promise<string[]> {
    const response = await this.api.post(`${this.route}create_lines`, data);
    return response.data;
  }
}
