import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';
import { IProductLocation } from '@nomalism-com/types/dist/modules/stock/productGoogleSheets/interface';

export default class Repository implements Nomalism.ProductGoogleSheets.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async basicSearch(
    params: Nomalism.ProductGoogleSheets.IBasicSearchRequest,
  ): Promise<
    Nomalism.shared.IPaginationResponse<Nomalism.ProductGoogleSheets.IBasicSearchResponse>
  > {
    const response = await this.api.get(`${this.route}basic_search`, {
      params,
    });
    return response.data;
  }

  async findReferenceAndIdForMobile(
    params: Nomalism.ProductGoogleSheets.IFindForMobileRequest,
  ): Promise<Nomalism.ProductGoogleSheets.IFindReferenceAndIdForMobileResponse | null> {
    const response = await this.api.get(`${this.route}reference_and_id_mobile`, {
      params,
    });
    return response.data;
  }

  async findDataForMobile(
    params: Nomalism.shared.IFindByIdRequest,
  ): Promise<Nomalism.ProductGoogleSheets.IFindDataForMobileResponse | null> {
    const response = await this.api.get(`${this.route}data_mobile/${params.id}`);
    return response.data;
  }

  async findStockForMobile(
    params: Nomalism.shared.IFindByIdRequest,
  ): Promise<Nomalism.ProductGoogleSheets.IFindStockForMobileResponse | null> {
    const response = await this.api.get(`${this.route}stock_mobile/${params.id}`);
    return response.data;
  }

  async findPreArtigo(
    params: Nomalism.ProductGoogleSheets.IFindProductSheetByReference,
  ): Promise<Nomalism.ProductGoogleSheets.IProductSheetCabideEntity | null> {
    const response = await this.api.get(`${this.route}pre_artigo`, {
      params,
    });
    return response.data;
  }

  async updatePricesFromPreArtigo(): Promise<void> {
    await this.api.get(`${this.route}update_prices_from_pre_artigo`);
  }

  async validateByRef(
    params: Nomalism.ProductGoogleSheets.IValidateByReferenceRequest,
  ): Promise<Nomalism.ProductGoogleSheets.IEntityMinified | null> {
    const response = await this.api.get(`${this.route}validate_by_ref`, {
      params,
    });
    return response.data;
  }

  async findSimilarProductsByName(
    params: Nomalism.ProductGoogleSheets.IFindSimilarProductsByNameRequest,
  ): Promise<Nomalism.ProductGoogleSheets.IFindSimilarProductsByNameResponse[]> {
    const response = await this.api.get(`${this.route}similar_products`, {
      params,
    });
    return response.data;
  }

  async findByProductType(
    params: Nomalism.ProductGoogleSheets.IFindByProductTypeRequest,
  ): Promise<Nomalism.ProductGoogleSheets.IFindByProductTypeResponse[]> {
    const response = await this.api.get(`${this.route}by_product_type`, {
      params,
    });

    return response.data;
  }

  async findById({
    id,
  }: Nomalism.shared.IFindByIdRequest): Promise<Nomalism.ProductGoogleSheets.IProductSheetEntity> {
    const response = await this.api.get(`${this.route}${id}`);
    return response.data;
  }

  async findByIdWithStock({
    id,
  }: Nomalism.shared.IFindByIdRequest): Promise<Nomalism.ProductGoogleSheets.IProductVirtuals> {
    const response = await this.api.get(`${this.route}${id}/with_stock`);
    return response.data;
  }

  async updateGoogleSheetsLinesProduct(body: Nomalism.shared.IFindByIdRequest[]): Promise<void> {
    const response = await this.api.put(`${this.route}update_google_sheets_lines_product`, body);
    return response.data;
  }

  async googleSheetsFieldsByRef(
    params: Nomalism.ProductGoogleSheets.IFindByReferenceRequest,
  ): Promise<string> {
    const response = await this.api.get(
      `${this.route}google_sheets_fields_by_ref/${params.reference}`,
    );
    return response.data;
  }

  async findRowPositionOnSheetsWithColumns(
    params: Nomalism.ProductGoogleSheets.IFindProductSheetByReference,
  ): Promise<string> {
    const response = await this.api.get(`${this.route}sheet_row_and_columns/${params.selector}`);
    return response.data;
  }

  async checkStockByIds(
    data: Nomalism.ProductGoogleSheets.ICheckStockByIdsRequest,
  ): Promise<Nomalism.ProductGoogleSheets.ICheckStockByIdsResponse[]> {
    const response = await this.api.post(`${this.route}check_stock_by_ids`, data);
    return response.data;
  }

  async create(body: Nomalism.ProductGoogleSheets.ICreateRequest): Promise<string> {
    const response = await this.api.post(`${this.route}`, body);
    return response.data;
  }

  async update(
    { id }: Nomalism.shared.IFindByIdRequest,
    body: Nomalism.ProductGoogleSheets.IUpdateRequest,
  ): Promise<void> {
    await this.api.put(`${this.route}${id}`, body);
  }

  async deleteOne(selector: Nomalism.shared.IFindByIdRequest): Promise<void> {
    await this.api.delete(`${this.route}${selector.id}`);
  }

  printLabelToPdfUrl({ id }: Nomalism.shared.IFindByIdRequest, token: string): string {
    const qs = new URLSearchParams();
    qs.set('token', token);
    return `${this.publicRoute}print_label_pdf/${id}?${qs.toString()}`;
  }

  printLabelToPdfBulk(ids: string[], token: string): string {
    const qs = new URLSearchParams();
    qs.set('token', token);
    qs.set('ids', ids.join(','));
    return `${this.publicRoute}print_label_pdf/bulk?${qs.toString()}`;
  }

  async updateDbFromSheets(): Promise<void> {
    await this.api.post(`${this.route}update_db_from_sheets`);
  }

  async partialLocation(body: IProductLocation): Promise<void> {
    await this.api.post(`${this.route}partial_location`, body);
  }

  async fullLocation(body: IProductLocation): Promise<void> {
    await this.api.post(`${this.route}full_location`, body);
  }
}
