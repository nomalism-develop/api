import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import IEntity = Nomalism.ProductImage.Entity;

export import ImageTypeEnum = Nomalism.ProductImage.ImageTypeEnum;

export import IImageType = Nomalism.ProductImage.IImageType;

export import imageTypes = Nomalism.ProductImage.imageTypes;

export import IFindRequest = Nomalism.ProductImage.IFindRequest;
export import IFindResponse = Nomalism.ProductImage.IFindResponse;

export import IFindPaginatedRequest = Nomalism.ProductImage.IFindPaginatedRequest;

export import IFindByOwnerIdRequest = Nomalism.shared.IFindByOwnerIdRequest;

export import ICreateRequest = Nomalism.ProductImage.ICreateRequest;

export import IUpdateRequest = Nomalism.ProductImage.IUpdateRequest;
export type IPaginationResponse =
  Nomalism.shared.IPaginationResponse<Nomalism.ProductImage.IFindResponse>;

export default class Repository implements Nomalism.ProductImage.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async find(params: IFindRequest): Promise<IFindResponse[]> {
    const response = await this.api.get(`${this.route}`, { params });
    return response.data;
  }

  async findPaginated(params: IFindPaginatedRequest): Promise<IPaginationResponse> {
    const response = await this.api.get(`${this.route}paginated`, { params });
    return response.data;
  }

  async findByOwnerId(params: IFindByOwnerIdRequest): Promise<IFindResponse[]> {
    const response = await this.api.get(`${this.route}by_owner`, {
      params,
    });
    return response.data;
  }

  async findById(selector: Nomalism.shared.IFindByIdRequest): Promise<IFindResponse | null> {
    const response = await this.api.get(`${this.route}${selector.id}`);
    return response.data;
  }

  async create(body: ICreateRequest): Promise<IEntity> {
    const response = await this.api.post(`${this.route}`, body);
    return response.data;
  }

  async update(
    selector: Nomalism.shared.IFindByIdRequest,
    body: IUpdateRequest,
  ): Promise<IEntity | null> {
    const response = await this.api.put(`${this.route}${selector.id}`, body);
    return response.data;
  }

  async deleteOne(selector: Nomalism.shared.IFindByIdRequest): Promise<IEntity | null> {
    const response = await this.api.delete(`${this.route}${selector.id}`);
    return response.data;
  }
}
