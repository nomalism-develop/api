import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import IFindRequest = Nomalism.Promotion.IFindRequest;
export import IFindResponse = Nomalism.Promotion.IFindResponse;

export import IFindPaginatedRequest = Nomalism.Promotion.IFindPaginatedRequest;

export import IFindByOwnerIdRequest = Nomalism.shared.IFindByOwnerIdRequest;

export import IFindMinifiedRequest = Nomalism.shared.IFindMinifiedRequest;
export import IFindMinifiedResponse = Nomalism.shared.IFindMinifiedResponse;

export import ICreateRequest = Nomalism.Promotion.ICreateRequest;
export import IEntity = Nomalism.Promotion.Entity;

export import IUpdateRequest = Nomalism.Promotion.IUpdateRequest;
export type IPaginationResponse =
  Nomalism.shared.IPaginationResponse<Nomalism.Promotion.IFindResponse>;

export default class Repository implements Nomalism.Promotion.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async find(params: IFindRequest): Promise<IFindResponse[]> {
    const response = await this.api.get(`${this.route}`, { params });
    return response.data;
  }

  async findPaginated(params: IFindPaginatedRequest): Promise<IPaginationResponse> {
    const response = await this.api.get(`${this.route}paginated`, { params });
    return response.data;
  }

  async findMinified(params?: IFindMinifiedRequest): Promise<IFindMinifiedResponse[]> {
    const response = await this.api.get(`${this.route}minified`, {
      params,
    });
    return response.data;
  }

  async findByOwnerId(params: IFindByOwnerIdRequest): Promise<Nomalism.Promotion.IFindResponse[]> {
    const response = await this.api.get(`${this.route}by_owner`, {
      params,
    });

    return response.data;
  }

  async findById(selector: Nomalism.shared.IFindByIdRequest): Promise<IFindResponse | null> {
    const response = await this.api.get(`${this.route}${selector.id}`);
    return response.data;
  }

  async create(body: ICreateRequest): Promise<IEntity> {
    const response = await this.api.post(`${this.route}`, body);
    return response.data;
  }

  async update(
    selector: Nomalism.shared.IFindByIdRequest,
    body: IUpdateRequest,
  ): Promise<IEntity | null> {
    const response = await this.api.put(`${this.route}${selector.id}`, body);
    return response.data;
  }

  async deleteOne(selector: Nomalism.shared.IFindByIdRequest): Promise<IEntity | null> {
    const response = await this.api.delete(`${this.route}${selector.id}`);
    return response.data;
  }

  getBestDiscount(
    promotions: Pick<
      Nomalism.ProductGoogleSheets.IProductPromotionVirtuals,
      'exclusive_to_location_id' | 'exclusive_to_client_id' | 'discount'
    >[],
    discount?: number,
    location_id?: string,
    client_id?: string,
    product_no_discount?: boolean,
  ) {
    const highestPromoDiscount = promotions
      .filter((promotion) => {
        const locationEligible = promotion.exclusive_to_location_id
          ? promotion.exclusive_to_location_id === location_id
          : true;

        const clientEligible = promotion.exclusive_to_client_id
          ? promotion.exclusive_to_client_id === client_id
          : true;

        return locationEligible && clientEligible;
      })
      .reduce(
        (highest, promotion) => (promotion.discount > highest ? promotion.discount : highest),
        0,
      );

    const clientDiscount = product_no_discount ? 0 : discount || 0;

    return highestPromoDiscount > clientDiscount ? highestPromoDiscount : clientDiscount;
  }
}
