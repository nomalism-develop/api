import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import IFindResponse = Nomalism.QueryList.IFindResponse;
export import ICreateRequest = Nomalism.QueryList.ICreateRequest;

export import IEntity = Nomalism.QueryList.Entity;

export import IUpdateRequest = Nomalism.QueryList.IUpdateRequest;

export import IExecuteRequest = Nomalism.QueryList.IExecuteRequest;

export import IExecuteResponse = Nomalism.QueryList.IExecuteResponse;

export import IExecuteOutput = Nomalism.QueryList.IExecuteOutput;

export const { executeOutput } = Nomalism.QueryList;

export default class Repository implements Nomalism.QueryList.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async find(): Promise<IFindResponse[]> {
    const response = await this.api.get(`${this.route}`);
    return response.data;
  }

  async findById(selector: Nomalism.shared.IFindByIdRequest): Promise<IFindResponse | null> {
    const response = await this.api.get(`${this.route}${selector.id}`);
    return response.data;
  }

  async create(body: ICreateRequest): Promise<IFindResponse> {
    const response = await this.api.post(`${this.route}`, body);
    return response.data;
  }

  async update(
    selector: Nomalism.shared.IFindByIdRequest,
    body: IUpdateRequest,
  ): Promise<IFindResponse | null> {
    const response = await this.api.put(`${this.route}${selector.id}`, body);
    return response.data;
  }

  async deleteOne(selector: Nomalism.shared.IFindByIdRequest): Promise<void> {
    const response = await this.api.delete(`${this.route}${selector.id}`);
    return response.data;
  }

  async execute(params: IExecuteRequest): Promise<IExecuteResponse> {
    const response = await this.api.get(`${this.route}execute`, { params });
    return response.data;
  }

  getQueryExecuteUrl({ query_id, output, token, ...data }: IExecuteRequest) {
    const qs = new URLSearchParams();

    qs.set('query_id', query_id);
    qs.set('output', output);
    if (token) {
      qs.set('token', token);
    }

    Object.entries(data).forEach(([key, value]) => {
      if (typeof value === 'undefined') return;
      if (value === null) return;
      qs.set(key, value.toString());
    });

    return `${this.route}execute?${qs.toString()}`;
  }
}
