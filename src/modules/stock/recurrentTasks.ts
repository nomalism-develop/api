import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export default class Repository implements Nomalism.RecurrentTasks.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async from_master(): Promise<string> {
    return this.api.post(`${this.route}from_master`);
  }

  async from_master_checked(): Promise<string> {
    return this.api.post(`${this.route}from_master`);
  }
}
