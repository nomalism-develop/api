import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export default class Repository implements Nomalism.SavedEmPicking.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async create(body: Nomalism.SavedEmPicking.ICreateRequest): Promise<string> {
    const response = await this.api.post(`${this.route}`, body);
    return response.data;
  }

  async createMany(body: Nomalism.SavedEmPicking.ICreateManyRequest): Promise<string[]> {
    const response = await this.api.post(`${this.route}many`, body);
    return response.data;
  }

  async deleteOne(selector: Nomalism.shared.IFindByIdRequest): Promise<void> {
    await this.api.delete(`${this.route}${selector.id}`);
  }

  async deleteMany(body: Nomalism.SavedEmPicking.IDeleteManyRequest): Promise<void> {
    await this.api.post(`${this.route}delete_many`, body);
  }
}
