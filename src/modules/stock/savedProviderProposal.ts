import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import IEntity = Nomalism.SavedProviderProposal.Entity;

export import ICreateRequest = Nomalism.SavedProviderProposal.ICreateRequest;
export import IUpdateRequest = Nomalism.SavedProviderProposal.IUpdateRequest;

export default class Repository implements Nomalism.SavedProviderProposal.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async findByProviderId(
    selector: Nomalism.shared.IFindByIdRequest,
  ): Promise<Nomalism.SavedProviderProposal.IFindManyResponse[]> {
    const response = await this.api.get(`${this.route}${selector.id}`);
    return response.data;
  }

  async create(body: ICreateRequest): Promise<void> {
    const response = await this.api.post(`${this.route}`, body);
    return response.data;
  }

  async update(selector: Nomalism.shared.IFindByIdRequest, body: IUpdateRequest): Promise<void> {
    const response = await this.api.put(`${this.route}${selector.id}`, body);
    return response.data;
  }

  async deleteOne(selector: Nomalism.shared.IFindByIdRequest): Promise<void> {
    const response = await this.api.delete(`${this.route}${selector.id}`);
    return response.data;
  }
}
