import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import IScheduleType = Nomalism.Schedule.IScheduleType;

export import IRunImmediatelyRequest = Nomalism.Schedule.IRunImmediatelyRequest;

export const { ScheduleTypeEnum, scheduleTypes, ScheduleTypeLabel } = Nomalism.Schedule;

export default class Repository implements Nomalism.Schedule.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async runImmediately(selector: IRunImmediatelyRequest): Promise<void> {
    await this.api.post(`${this.route}${selector.scheduleType}`);
  }
}
