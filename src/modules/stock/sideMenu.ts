import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import IFindByDocumentLineStateRequest = Nomalism.SideMenu.IFindByDocumentLineStateRequest;
export import IFindByDocumentLineStateResponse = Nomalism.SideMenu.IFindByDocumentLineStateResponse;
export import IFindByStateRequest = Nomalism.SideMenu.IFindByStateRequest;
export import IFindByStateResponse = Nomalism.SideMenu.IFindByStateResponse;
export import IFindSideMenuCountersResponse = Nomalism.SideMenu.IFindSideMenuCountersResponse;

export default class Repository implements Nomalism.SideMenu.IController {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async findSideMenuStoreOperators(): Promise<string[]> {
    const response = await this.api.get(`${this.route}side_menu_store_operators`);
    return response.data;
  }

  async findByDocumentLineState(
    params: IFindByDocumentLineStateRequest,
  ): Promise<IFindByDocumentLineStateResponse[]> {
    const response = await this.api.get(`${this.route}by_document_line_state`, {
      params,
    });
    return response.data;
  }

  async findByState(params: IFindByStateRequest): Promise<IFindByStateResponse[]> {
    const response = await this.api.get(`${this.route}find_by_state`, {
      params,
    });
    return response.data;
  }

  async findSideMenuCounters(): Promise<IFindSideMenuCountersResponse> {
    const response = await this.api.get(`${this.route}counters`);
    return response.data;
  }
}
