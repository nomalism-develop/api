import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import IFindByIdNumberRequest = Nomalism.shared.IFindByIdNumberRequest;
export import IFindByProviderResponse = Nomalism.StockMovement.IFindByProviderResponse;

export import IFindByOwnerIdRequest = Nomalism.shared.IFindByOwnerIdRequest;
export import IFindByOwnerIdResponse = Nomalism.StockMovement.IFindByOwnerIdResponse;

export import ExportFormatEnum = Nomalism.StockMovement.ExportFormatEnum;
export import IExportFormat = Nomalism.StockMovement.IExportFormat;
export import exportFormats = Nomalism.StockMovement.exportFormats;
export import ExportVersionEnum = Nomalism.StockMovement.ExportVersionEnum;
export import IExportVersion = Nomalism.StockMovement.IExportVersion;
export import exportVersions = Nomalism.StockMovement.exportVersions;
export import IExportRequest = Nomalism.StockMovement.IExportRequest;
export import IExportCsvResponse = Nomalism.StockMovement.IExportCsvResponse;

export default class Repository implements Nomalism.StockMovement.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async findByProvider({ id }: IFindByIdNumberRequest): Promise<IFindByProviderResponse[]> {
    const response = await this.api.get(`${this.route}${id}`);
    return response.data;
  }

  async findByOwnerId(params: IFindByOwnerIdRequest): Promise<IFindByOwnerIdResponse[]> {
    const response = await this.api.get(`${this.route}by_owner`, { params });
    return response.data;
  }

  async exportCsv(params: IExportRequest): Promise<IExportCsvResponse[]> {
    const response = await this.api.get(`${this.route}csv`, { params });
    return response.data;
  }

  getStockCsvUrl({ date, version, format, token }: IExportRequest): string {
    const qs = new URLSearchParams();
    qs.set('date', date.toISOString());
    qs.set('version', version);
    qs.set('format', format);
    qs.set('token', token);
    return `${this.publicRoute}csv?${qs.toString()}`;
  }

  async findStockMovementPaginated(
    params: Nomalism.StockMovement.IFindStockMovementPaginatedRequest,
  ): Promise<Nomalism.shared.IPaginationResponse<IFindByOwnerIdResponse>> {
    const response = await this.api.get(`${this.route}stock_movement_paginated`, { params });
    return response.data;
  }
}
