import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export default class Repository implements Nomalism.Task.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }
  async find(selector: Nomalism.Task.IFindTasksRequest): Promise<Nomalism.Task.IFindResponse[]> {
    return this.api.get(`${this.route}${selector}`);
  }

  async findWithMessages(
    selector: Nomalism.shared.IFindByIdRequest,
  ): Promise<Nomalism.Task.IFindWithMessagesResponse> {
    return this.api.get(`${this.route}with_messages`, {
      params: selector,
    });
  }

  async create(body: Nomalism.Task.ICreateRequest): Promise<Nomalism.Task.Entity> {
    const response = await this.api.post(`${this.route}`, body);
    return response.data;
  }

  async update(
    selector: Nomalism.shared.IFindByIdRequest,
    body: Nomalism.Task.IUpdateRequest,
  ): Promise<void> {
    await this.api.put(`${this.route}${selector.id}`, body);
  }

  async delete(selector: Nomalism.shared.IFindByIdRequest): Promise<void> {
    await this.api.delete(`${this.route}${selector.id}`);
  }
  async findByOwnerId(
    selector: Nomalism.shared.IFindByOwnerIdRequest,
  ): Promise<Nomalism.Task.IFindByOwnerResponse[]> {
    const response = await this.api.delete(`${this.route}by_owner/${selector.owner_id}`);
    return response.data;
  }
}
