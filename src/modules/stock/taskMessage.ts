import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export default class Repository implements Nomalism.TaskMessage.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async create(body: Nomalism.TaskMessage.ICreateRequest): Promise<Nomalism.TaskMessage.Entity> {
    const response = await this.api.post(`${this.route}`, body);
    return response.data;
  }

  async update(
    selector: Nomalism.shared.IFindByIdRequest,
    body: Nomalism.TaskMessage.IUpdateRequest,
  ): Promise<void> {
    await this.api.put(`${this.route}${selector.id}`, body);
  }

  async delete(selector: Nomalism.shared.IFindByIdRequest): Promise<void> {
    await this.api.delete(`${this.route}${selector.id}`);
  }
}
