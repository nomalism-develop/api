import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export default class Repository implements Nomalism.TaskRead.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async create(body: Nomalism.TaskRead.ICreateRequest): Promise<void> {
    await this.api.post(`${this.route}`, body);
  }

  async delete(params: Nomalism.TaskRead.IDeleteRequest): Promise<void> {
    await this.api.delete(`${this.route}`, { data: params });
  }
}
