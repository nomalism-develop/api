import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import IEntity = Nomalism.ClientCurrentAccount.Entity;

export import IFindRequest = Nomalism.ClientCurrentAccount.IFindRequest;

export import IFindFaturasPorPagarResponse = Nomalism.ClientCurrentAccount.IFindFaturasPorPagarResponse;
export import IPagarFaturaRequest = Nomalism.ClientCurrentAccount.IPagarFaturaRequest;

export import IFindFaturasParaDevolucaoResponse = Nomalism.ClientCurrentAccount.IFindFaturasParaDevolucaoResponse;

export import ILinhasFaturasParaDevolucao = Nomalism.ClientCurrentAccount.ILinhasFaturasParaDevolucao;

export import IDevolverRequest = Nomalism.ClientCurrentAccount.IDevolverRequest;

export import IBasicSearchRequest = Nomalism.ClientCurrentAccount.IBasicSearchRequest;
export import IBasicSearchResponse = Nomalism.ClientCurrentAccount.IBasicSearchResponse;

export import IMultipleRegularizations = Nomalism.ClientCurrentAccount.IMultipleRegularizations;

export default class Repository implements Nomalism.ClientCurrentAccount.IApi {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async findByOwnerId(params: IFindRequest): Promise<Nomalism.shared.IPaginationResponse<IEntity>> {
    const response = await this.api.get(`${this.route}by_owner`, {
      params,
    });
    return response.data;
  }

  async findFaturaPorPagar(
    params: Nomalism.shared.IFindByIdRequest,
  ): Promise<IFindFaturasPorPagarResponse> {
    const response = await this.api.get(`${this.route}fatura_por_pagar`, {
      params,
    });
    return response.data;
  }

  async pagarFatura(data: IPagarFaturaRequest): Promise<void> {
    await this.api.post(`${this.route}pagar_fatura`, data);
  }

  async findFaturaParaDevolver(
    params: Nomalism.shared.IFindByIdRequest,
  ): Promise<IFindFaturasParaDevolucaoResponse> {
    const response = await this.api.get(`${this.route}fatura_para_devolver`, {
      params,
    });
    return response.data;
  }

  async devolver(data: IDevolverRequest): Promise<void> {
    await this.api.post(`${this.route}devolver`, data);
  }

  async findByBasicSearch(
    params: IBasicSearchRequest,
  ): Promise<Nomalism.shared.IPaginationResponse<IBasicSearchResponse>> {
    const response = await this.api.get(`${this.route}basic_search`, {
      params,
    });
    return response.data;
  }

  async regularizarMultiplosDocumentos(data: IMultipleRegularizations): Promise<void> {
    await this.api.post(`${this.route}multiple_documents`, {
      data,
    });
  }
}
