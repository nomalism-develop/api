import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import IFindOneResponse = Nomalism.DocumentHeader.IFindOneResponse;

export import IFindRequest = Nomalism.DocumentHeader.IFindRequest;

export import IFindMinifiedResponse = Nomalism.shared.IFindMinifiedResponse;

export import IBasicSearchRequest = Nomalism.DocumentHeader.IBasicSearchRequest;

export import IBasicSearchResponse = Nomalism.DocumentHeader.IBasicSearchResponse;

export import ICreateRequest = Nomalism.DocumentHeader.ICreateRequest;
export import ICreateResponse = Nomalism.DocumentHeader.ICreateResponse;

export import IUpdateRequest = Nomalism.DocumentHeader.IUpdateRequest;

export import IEntity = Nomalism.DocumentHeader.Entity;

export import IStartDocumentHeaderVirtuals = Nomalism.DocumentHeader.IStartDocumentHeaderVirtuals;

export import IDocumentHeaderClientVirtuals = Nomalism.DocumentHeader.IDocumentHeaderClientVirtuals;

export import IDocumentHeaderProviderVirtuals = Nomalism.DocumentHeader.IDocumentHeaderProviderVirtuals;

export import ICreateFromHeaderMaturityDate = Nomalism.DocumentHeader.ICreateFromHeaderMaturityDate;

export import ICreateFromHeaderRequest = Nomalism.DocumentHeader.ICreateFromHeaderRequest;

export import IGetActionsResponse = Nomalism.DocumentHeader.IGetActionsResponse;

export import ITaskCluster = Nomalism.shared.ITaskCluster;

export import IUnpaidDocumentResponse = Nomalism.DocumentHeader.IUnpaidDocumentResponse;

export import ITransferOwnershipRequest = Nomalism.DocumentHeader.ITransferOwnershipRequest;

export import IUpdateManyWithPersona = Nomalism.DocumentHeader.IUpdateManyWithPersona;

export import IFindResponse = Nomalism.DocumentHeader.IFindResponse;

export import ISendClientNotificationRequest = Nomalism.DocumentHeader.ISendClientNotificationRequest;

export import IDocumentPdfRequest = Nomalism.DocumentHeader.IDocumentPdfRequest;

export import IFindByTypeRequest = Nomalism.DocumentHeader.IFindByTypeRequest;
export import IFindByTypeResponse = Nomalism.DocumentHeader.IFindByTypeResponse;

export import IDocumentListResponse = Nomalism.DocumentHeader.IDocumentListResponse;

export default class Repository implements Nomalism.DocumentHeader.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async findOne(selector: Nomalism.shared.IFindByIdRequest): Promise<IFindOneResponse | null> {
    const response = await this.api.get(`${this.route}${selector.id}`);
    return response.data;
  }

  async findStart(selector: Nomalism.shared.IFindByIdRequest): Promise<IFindOneResponse | null> {
    const response = await this.api.get(`${this.route}start/${selector.id}`);
    return response.data;
  }

  async findStartVirtual(
    selector: Nomalism.shared.IFindByIdRequest,
  ): Promise<IStartDocumentHeaderVirtuals | null> {
    const response = await this.api.get(`${this.route}start_virtual/${selector.id}`);
    return response.data;
  }

  async findByBasicSearch(
    params: IBasicSearchRequest,
  ): Promise<Nomalism.shared.IPaginationResponse<IBasicSearchResponse>> {
    const response = await this.api.get(`${this.route}basic_search`, {
      params,
    });
    return response.data;
  }

  async findByType(params: IFindByTypeRequest): Promise<IFindByTypeResponse[]> {
    const response = await this.api.get(`${this.route}by_type`, { params });
    return response.data;
  }

  async find(params: IFindRequest): Promise<IFindResponse[]> {
    const response = await this.api.get(`${this.route}`, { params });
    return response.data;
  }

  async findClientUnpaidCredit(
    params: Nomalism.shared.IFindByOwnerIdRequest,
  ): Promise<IUnpaidDocumentResponse[]> {
    const response = await this.api.get(`${this.route}unpaid_credit`, {
      params,
    });
    return response.data;
  }

  async findClientUnpaidDebit(
    params: Nomalism.shared.IFindByOwnerIdRequest,
  ): Promise<IUnpaidDocumentResponse[]> {
    const response = await this.api.get(`${this.route}unpaid_debit`, {
      params,
    });
    return response.data;
  }

  async findUnpaidCommissions(
    params: Nomalism.shared.IFindByOwnerIdRequest,
  ): Promise<IUnpaidDocumentResponse[]> {
    const response = await this.api.get(`${this.route}unpaid_commissions`, {
      params,
    });
    return response.data;
  }

  async create(body: ICreateRequest): Promise<ICreateResponse> {
    const response = await this.api.post(`${this.route}`, body);
    return response.data;
  }

  async createFromHeader(body: ICreateFromHeaderRequest): Promise<ITaskCluster[]> {
    const response = await this.api.post(`${this.route}from_header`, body);
    return response.data;
  }

  async update(selector: Nomalism.shared.IFindByIdRequest, body: IUpdateRequest): Promise<void> {
    const response = await this.api.put(`${this.route}${selector.id}`, body);
    return response.data;
  }

  async deleteOne(selector: Nomalism.shared.IFindByIdRequest): Promise<void> {
    await this.api.delete(`${this.route}${selector.id}`);
  }

  async getActions(selector: Nomalism.shared.IFindByIdRequest): Promise<IGetActionsResponse[]> {
    const response = await this.api.get(`${this.route}get_actions/${selector.id}`);
    return response.data;
  }

  async transferClientOwnership(data: ITransferOwnershipRequest): Promise<void> {
    await this.api.put(`${this.route}transfer_client_ownership`, data);
  }

  async updateManyWithPersona(data: IUpdateManyWithPersona): Promise<void> {
    await this.api.put(`${this.route}update_many_with_persona`, data);
  }

  async sendClientNotification(
    { id }: Nomalism.shared.IFindByIdRequest,
    data: ISendClientNotificationRequest,
  ): Promise<void> {
    await this.api.post(`${this.route}${id}/send_client_notification`, data);
  }

  async markUnsentClientNotification({ id }: Nomalism.shared.IFindByIdRequest): Promise<void> {
    await this.api.post(`${this.route}${id}/mark_unsent_client_notification`);
  }

  async documentPdf({ id }: Nomalism.shared.IFindByIdRequest): Promise<string> {
    const result = await this.api.post(`${this.route}document_pdf/${id}`);
    return result.data;
  }

  async documentPdfStream(
    { id }: Nomalism.shared.IFindByIdRequest,
    params?: IDocumentPdfRequest,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
  ): Promise<any> {
    const result = await this.api.get(`${this.route}document_pdf/${id}`, {
      params,
      responseType: 'arraybuffer',
    });
    return result.data;
  }

  getDocumentPdfUrl({ id }: Nomalism.shared.IFindByIdRequest, data?: IDocumentPdfRequest): string {
    const qs = new URLSearchParams();
    if (data?.token) {
      qs.set('token', data.token);
    }
    return `${this.route}document_pdf/${id}?${qs.toString()}`;
  }

  async documentList({
    id,
  }: Nomalism.shared.IFindByIdRequest): Promise<Nomalism.DocumentHeader.IDocumentListResponse[]> {
    const result = await this.api.post(`${this.route}document_list/${id}`);
    return result.data;
  }

  async getAllRelatedDocumentHeaderIds({
    id,
  }: Nomalism.shared.IFindByIdRequest): Promise<string[]> {
    const result = await this.api.get(`${this.route}related/${id}`);
    return result.data;
  }

  async findStartDocumentHeaderSiblings({
    id,
  }: Nomalism.shared.IFindByIdRequest): Promise<Nomalism.DocumentHeader.IFindStartDocumentHeaderSiblingsResponse> {
    const result = await this.api.get(`${this.route}siblings/${id}`);
    return result.data;
  }
}
