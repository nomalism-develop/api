import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import IDocumentLineVirtuals = Nomalism.DocumentLine.IDocumentLineVirtuals;

export import IFindByOwnerIdRequest = Nomalism.shared.IFindByOwnerIdRequest;

export import ICreateRequest = Nomalism.DocumentLine.ICreateRequest;

export import IUpdateRequest = Nomalism.DocumentLine.IUpdateRequest;

export import IUpdateDataRequest = Nomalism.DocumentLine.IUpdateDataRequest;
export import IPrintLabelParamsRequest = Nomalism.DocumentLine.IPrintLabelParamsRequest;

export import IUpdateManyRequest = Nomalism.DocumentLine.IUpdateManyRequest;

export import IEntity = Nomalism.DocumentLine.Entity;

export import IEntityExtended = Nomalism.DocumentLine.IEntityExtended;

export import IDataKeys = Nomalism.DocumentLine.IDataKeys;

export import IDataPayload = Nomalism.DocumentLine.IDataPayload;

export import IData = Nomalism.DocumentLine.IData;

export default class Repository implements Nomalism.DocumentLine.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async create(body: ICreateRequest[]): Promise<Nomalism.DocumentLineAssoc.IEntityExtended[]> {
    const response = await this.api.post(`${this.route}`, body);
    return response.data;
  }

  async update(selector: Nomalism.shared.IFindByIdRequest, body: IUpdateRequest): Promise<void> {
    await this.api.put(`${this.route}${selector.id}`, body);
  }

  async updateIndexes(document_line_ids: string[]): Promise<void> {
    await this.api.put(`${this.route}update_indexes`, document_line_ids);
  }

  async updateData(
    data: IUpdateDataRequest,
  ): Promise<Nomalism.DocumentLineAssoc.IEntityWithDocumentLine[]> {
    const response = await this.api.post(`${this.route}update_data`, data);
    return response.data;
  }

  async updateMany(data: IUpdateManyRequest): Promise<void> {
    const response = await this.api.post(`${this.route}update_many`, data);
    return response.data;
  }

  async updateManyByDocumentHeader(
    params: Nomalism.shared.IFindByIdRequest,
    data: Nomalism.DocumentLine.IUpdateManyByDocumentHeaderRequest,
  ): Promise<void> {
    await this.api.post(`${this.route}update_many_by_document_header_id`, data, { params });
  }

  async deleteMany(document_line_ids: string[]): Promise<void> {
    await this.api.delete(`${this.route}`, { data: document_line_ids });
  }

  printLabelToPdfUrl({ document_line_ids }: IPrintLabelParamsRequest, token: string): string {
    const qs = new URLSearchParams();
    qs.set('token', token);
    return `${this.route}print_label_pdf/${document_line_ids}?${qs.toString()}`;
  }
}
