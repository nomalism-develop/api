import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import IEntity = Nomalism.DocumentLineAssoc.Entity;

export import IEntityWithDocumentLine = Nomalism.DocumentLineAssoc.IEntityWithDocumentLine;

export import IFindByOwnerIdRequest = Nomalism.shared.IFindByOwnerIdRequest;
export import IEntityExtended = Nomalism.DocumentLineAssoc.IEntityExtended;

export import ICreateRequest = Nomalism.DocumentLineAssoc.ICreateRequest;

export import IUpdateRequest = Nomalism.DocumentLineAssoc.IUpdateRequest;

export default class Repository implements Nomalism.DocumentLineAssoc.IApi {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async findByOwnerId(params?: IFindByOwnerIdRequest): Promise<IEntityExtended[]> {
    const response = await this.api.get(`${this.route}by_owner`, { params });
    return response.data;
  }

  async findVirtualsByOwnerId(
    params?: IFindByOwnerIdRequest,
  ): Promise<Nomalism.DocumentLine.IDocumentLineVirtuals[]> {
    const response = await this.api.get(`${this.route}virtuals_by_owner`, {
      params,
    });
    return response.data;
  }

  async create(body: ICreateRequest[]): Promise<IEntityWithDocumentLine[]> {
    const response = await this.api.post(`${this.route}`, body);
    return response.data;
  }

  async update(
    selector: Nomalism.shared.IFindByIdRequest,
    body: IUpdateRequest,
  ): Promise<IEntityWithDocumentLine> {
    const response = await this.api.put(`${this.route}${selector.id}`, body);
    return response.data;
  }
}
