import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import IDocumentTypeUserType = Nomalism.DocumentType.IDocumentTypeUserType;

export import IDocumentTypeCodeType = Nomalism.DocumentType.IDocumentTypeCodeType;

export import DocumentTypeUserTypeEnum = Nomalism.DocumentType.DocumentTypeUserTypeEnum;

export import documentTypeUserTypes = Nomalism.DocumentType.documentTypeUserTypes;

export import IFindRequest = Nomalism.DocumentType.IFindRequest;
export import IFindResponse = Nomalism.DocumentType.IFindResponse;

export import IFindMinifiedRequest = Nomalism.shared.IFindMinifiedRequest;
export import IFindMinifiedResponse = Nomalism.shared.IFindMinifiedResponse;

export import IEntityExtended = Nomalism.DocumentType.IEntityExtended;

export import ICreateRequest = Nomalism.DocumentType.ICreateRequest;

export import IEntity = Nomalism.DocumentType.Entity;

export import IUpdateRequest = Nomalism.DocumentType.IUpdateRequest;

export default class Repository implements Nomalism.DocumentType.IApi {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async find(params?: IFindRequest): Promise<IFindResponse[]> {
    const response = await this.api.get(`${this.route}`, { params });
    return response.data;
  }

  async findMinified(params?: IFindMinifiedRequest): Promise<IFindMinifiedResponse[]> {
    const response = await this.api.get(`${this.route}minified`, { params });
    return response.data;
  }

  async findDetailed(params?: IFindRequest): Promise<IEntityExtended[]> {
    const response = await this.api.get(`${this.route}detailed`, { params });
    return response.data;
  }

  async create(body: ICreateRequest): Promise<IEntityExtended> {
    const response = await this.api.post(`${this.route}`, body);
    return response.data;
  }

  async update(
    selector: Nomalism.shared.IFindByIdNumberRequest,
    body: IUpdateRequest,
  ): Promise<IEntityExtended | null> {
    const response = await this.api.put(`${this.route}${selector.id}`, body);
    return response.data;
  }

  async deleteOne(
    selector: Nomalism.shared.IFindByIdNumberRequest,
  ): Promise<IEntityExtended | null> {
    const response = await this.api.delete(`${this.route}${selector.id}`);
    return response.data;
  }
}
