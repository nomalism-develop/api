import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import IEntity = Nomalism.ExternalDocumentHeader.Entity;

export import ICreateRequest = Nomalism.ExternalDocumentHeader.ICreateRequest;
export import IUpdateRequest = Nomalism.ExternalDocumentHeader.IUpdateRequest;

export import IFindRequest = Nomalism.ExternalDocumentHeader.IFindRequest;
export import IFindResponse = Nomalism.ExternalDocumentHeader.IFindResponse;

export import IFindPaginatedRequest = Nomalism.ExternalDocumentHeader.IFindPaginatedRequest;
export import IFindWithPaginationResponse = Nomalism.ExternalDocumentHeader.IFindWithPaginationResponse;

export import IFindByIdRequest = Nomalism.shared.IFindByIdRequest;
export import IFindByIdResponse = Nomalism.ExternalDocumentHeader.IFindByIdResponse;

export import IFindByOwnerIdRequest = Nomalism.shared.IFindByOwnerIdRequest;

export import ISendEmailDocumentRequest = Nomalism.ExternalDocumentHeader.ISendEmailDocumentRequest;

export default class Repository implements Nomalism.ExternalDocumentHeader.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async create(body: ICreateRequest): Promise<IEntity> {
    const response = await this.api.post(`${this.route}`, body);
    return response.data;
  }

  async update(selector: IFindByIdRequest, body: IUpdateRequest): Promise<IEntity | null> {
    const response = await this.api.put(`${this.route}${selector.id}`, body);
    return response.data;
  }

  async findById(selector: IFindByIdRequest): Promise<IFindByIdResponse | null> {
    const response = await this.api.get(`${this.route}${selector.id}`);
    return response.data;
  }

  async findByOwnerId(
    params?: IFindByOwnerIdRequest,
  ): Promise<Nomalism.ExternalDocumentHeader.IFindByOwnerIdItemWithVersions[]> {
    const response = await this.api.get(`${this.route}by_owner`, { params });
    return response.data;
  }

  async publicFindByOwnerId(
    params?: Nomalism.shared.IFindByOwnerIdRequest,
  ): Promise<Nomalism.ExternalDocumentHeader.IPublicFindByOwnerIdResponse[]> {
    const response = await this.api.get(`${this.route}public_by_owner`, {
      params,
    });
    return response.data;
  }

  async find(params?: IFindRequest): Promise<IFindResponse[]> {
    const response = await this.api.get(`${this.route}`, { params });
    return response.data;
  }

  async findPaginated(params: IFindPaginatedRequest): Promise<IFindWithPaginationResponse> {
    const response = await this.api.get(`${this.route}paginated`, { params });
    return response.data;
  }

  async deleteOne(params?: IFindByIdRequest): Promise<IEntity | null> {
    const response = await this.api.get(`${this.route}`, { params });
    return response.data;
  }

  async sendEmailDocument(body: ISendEmailDocumentRequest): Promise<void> {
    await this.api.post(`${this.route}sendEmail`, body);
  }

  async createDocumentHeaderNote(
    data: Nomalism.ExternalDocumentHeader.DocumentHeaderNoteCreateRequest[],
  ): Promise<Nomalism.ExternalDocumentHeader.DocumentHeaderNote[]> {
    const response = await this.api.post(`${this.route}public_document_header_note`, data);
    return response.data;
  }
  async updateDocumentHeaderNote(
    { id }: Nomalism.shared.IFindByIdRequest,
    data: Nomalism.ExternalDocumentHeader.DocumentHeaderNoteUpdateRequest[],
  ): Promise<Nomalism.ExternalDocumentHeader.DocumentHeaderNote[]> {
    const response = await this.api.put(`${this.route}public_document_header_note/${id}`, data);
    return response.data;
  }
}
