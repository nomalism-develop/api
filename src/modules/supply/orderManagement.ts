import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import IFindProviderProductsRequest = Nomalism.OrderManagement.IFindProviderProductsRequest;
export import IFindProviderProductsResponse = Nomalism.OrderManagement.IFindProviderProductsResponse;
export import IFindAllProviderProductsResponse = Nomalism.OrderManagement.IFindAllProviderProductsResponse;

export default class Repository implements Nomalism.OrderManagement.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async findProviderProducts(
    params: IFindProviderProductsRequest,
  ): Promise<IFindProviderProductsResponse[]> {
    const response = await this.api.get(`${this.route}provider_products`, {
      params,
    });
    return response.data;
  }

  async findAllProviderProducts(): Promise<IFindAllProviderProductsResponse[]> {
    const response = await this.api.get(`${this.route}all_provider_products`);
    return response.data;
  }
}
