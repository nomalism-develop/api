import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import IFindRequest = Nomalism.Payment.IFindRequest;
export import IFindResponse = Nomalism.Payment.IFindResponse;

export import IFindPaginatedRequest = Nomalism.Payment.IFindPaginatedRequest;

export import IFindByOwnerIdRequest = Nomalism.shared.IFindByOwnerIdRequest;
export import IFindByIdRequest = Nomalism.shared.IFindByIdRequest;

export import ICurrentAccountNotes = Nomalism.Payment.ICurrentAccountNotes;
export import IFindCurrentAccountByOwnerIdResponse = Nomalism.Payment.IFindCurrentAccountByOwnerIdResponse;

export import ICreateForThisDocumentHeaderRequest = Nomalism.Payment.ICreateForThisDocumentHeaderRequest;

export import ICreateRequest = Nomalism.Payment.ICreateRequest;

export import IEntity = Nomalism.Payment.Entity;

export import IExportPaymentsRequest = Nomalism.Payment.IExportPaymentsRequest;
export import IPaymentsBatchResponse = Nomalism.Payment.IPaymentsBatchResponse;

export import IUnpaidPurchasesRequest = Nomalism.Payment.IUnpaidPurchasesRequest;
export import IUnpaidPurchasesResponse = Nomalism.Payment.IUnpaidPurchasesResponse;
export import IUnpaidByDatePurchasesRequest = Nomalism.Payment.IUnpaidByDatePurchasesRequest;
export import IFindProvidersWithClientIdResponse = Nomalism.Payment.IFindProvidersWithClientIdResponse;

export import IProviderPaymentDocument = Nomalism.Payment.IProviderPaymentDocument;
export import IProviderPaymentRequest = Nomalism.Payment.IProviderPaymentRequest;

export import IPaymentsNotExportedRequest = Nomalism.Payment.IPaymentsNotExportedRequest;
export import IPaymentsNotExportedResponse = Nomalism.Payment.IPaymentsNotExportedResponse;

export import IExportCurrentAccountRequest = Nomalism.Payment.IExportCurrentAccountRequest;

export type IPaginationResponse =
  Nomalism.shared.IPaginationResponse<Nomalism.Payment.IFindResponse>;

export default class Repository implements Nomalism.Payment.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async find(params: IFindRequest): Promise<IFindResponse[]> {
    const response = await this.api.get(`${this.route}`, { params });
    return response.data;
  }

  async findPaginated(params: IFindPaginatedRequest): Promise<IPaginationResponse> {
    const response = await this.api.get(`${this.route}paginated`, { params });
    return response.data;
  }

  async findByOwnerId(params: IFindByOwnerIdRequest): Promise<IFindResponse[]> {
    const response = await this.api.get(`${this.route}by_owner`, {
      params,
    });
    return response.data;
  }

  async findById(selector: IFindByIdRequest): Promise<IEntity | null> {
    const response = await this.api.get(`${this.route}${selector.id}`);
    return response.data;
  }

  async deleteBatch(selector: IFindByIdRequest): Promise<void> {
    const response = await this.api.delete(`${this.route}delete_batch${selector.id}`);
    return response.data;
  }

  async findCurrentAccountByOwnerId(
    params: Nomalism.shared.IFindByOwnerIdRequest,
  ): Promise<IFindCurrentAccountByOwnerIdResponse[]> {
    const response = await this.api.get(`${this.route}current_account`, {
      params,
    });
    return response.data;
  }

  async findBalanceByOwnerId(params: Nomalism.shared.IFindByOwnerIdRequest): Promise<number> {
    const response = await this.api.get(`${this.route}balance`, {
      params,
    });
    return response.data;
  }

  async findSettledMaterialEntrance(
    params: IUnpaidPurchasesRequest,
  ): Promise<IUnpaidPurchasesResponse[]> {
    const response = await this.api.get(`${this.route}settled_material_entrance`, {
      params,
    });
    return response.data;
  }

  async findSettledMaterialEntranceProviders(): Promise<IFindProvidersWithClientIdResponse[]> {
    const response = await this.api.get(`${this.route}settled_material_entrance_providers`);
    return response.data;
  }

  async createProvidersPayments(body: IProviderPaymentRequest[]): Promise<string[]> {
    const response = await this.api.post(`${this.route}provider`, body);
    return response.data;
  }

  async findPaymentsNotExported(
    params: IPaymentsNotExportedRequest,
  ): Promise<IPaymentsNotExportedResponse[]> {
    const response = await this.api.get(`${this.route}not_exported`, {
      params,
    });
    return response.data;
  }

  async findPaymentBatches(): Promise<IPaymentsBatchResponse[]> {
    const response = await this.api.get(`${this.route}batches`);
    return response.data;
  }

  async updateDownloadedBatchNumber(params: Nomalism.shared.IFindByIdRequest): Promise<void> {
    await this.api.put(`${this.route}downloaded_batch_number/${params}`);
  }

  async exportCurrentAccount(
    params: IExportCurrentAccountRequest,
  ): Promise<IFindCurrentAccountByOwnerIdResponse[]> {
    const response = await this.api.get(`${this.publicRoute}export_current_account`, {
      params,
    });
    return response.data;
  }

  async findSettledMaterialEntranceByDate(
    params: IUnpaidByDatePurchasesRequest,
  ): Promise<IUnpaidPurchasesResponse[]> {
    const response = await this.api.get(`${this.route}settled_material_entrance_by_date`, {
      params,
    });
    return response.data;
  }

  async exportPayments(body: IExportPaymentsRequest[]): Promise<void> {
    await this.api.post(`${this.route}export_payments`, body);
  }

  getExportCurrentAccountUrl({
    owner_id,
    start_date,
    end_date,
    output,
    token,
  }: IExportCurrentAccountRequest): string {
    const qs = new URLSearchParams();
    qs.set('owner_id', owner_id);
    qs.set('output', output);
    if (start_date) qs.set('start_date', start_date.toISOString());
    if (end_date) qs.set('end_date', end_date.toISOString());
    qs.set('token', token);
    return `${this.publicRoute}export_current_account?${qs.toString()}`;
  }

  async findCurrentAccountPaginated(
    params: Nomalism.Payment.IFindCurrentAccountPaginatedRequest,
  ): Promise<
    Nomalism.shared.IPaginationResponse<Nomalism.Payment.IFindCurrentAccountByOwnerIdResponse>
  > {
    const response = await this.api.get(`${this.route}current_account_paginated`, {
      params,
    });
    return response.data;
  }
}
