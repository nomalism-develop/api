import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import IFindResponse = Nomalism.PreSale.IFindResponse;
export import IFindResponseWithCount = Nomalism.PreSale.IFindResponseWithCount;

export import ICreateRequest = Nomalism.PreSale.ICreateRequest;

export import IProductMinified = Nomalism.PreSale.IPreSaleProduct;
export import IEntityExtended = Nomalism.PreSale.IEntityExtended;

export import IUpdateRequest = Nomalism.PreSale.IUpdateRequest;
export import IUpdatePreSaleQuantityRequest = Nomalism.PreSale.IUpdatePreSaleQuantityRequest;

export import IRemovePreSaleProductRequest = Nomalism.PreSale.IRemovePreSaleProductRequest;

export import IImportToClientProposalRequest = Nomalism.PreSale.IImportToClientProposalRequest;

export import IPreSaleFindByUser = Nomalism.PreSale.IPreSaleFindByUser;

export type IPaginationResponse =
  Nomalism.shared.IPaginationResponse<Nomalism.PreSale.IFindResponse>;

export default class Repository implements Nomalism.PreSale.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async find(selector: Nomalism.PreSale.IPreSaleFindByUser): Promise<IFindResponseWithCount[]> {
    const response = await this.api.get(`${this.route}by_user/${selector.created_by}`);
    return response.data;
  }

  async findById(selector: Nomalism.shared.IFindByIdRequest): Promise<IEntityExtended | null> {
    const response = await this.api.get(`${this.route}${selector.id}`);
    return response.data;
  }

  async findPending(): Promise<IEntityExtended[]> {
    const response = await this.api.get(`${this.route}pending`);
    return response.data;
  }

  async create(body: ICreateRequest): Promise<IFindResponse> {
    const response = await this.api.post(`${this.route}`, body);
    return response.data;
  }

  async update(
    selector: Nomalism.shared.IFindByIdRequest,
    body: IUpdateRequest,
  ): Promise<IFindResponse | null> {
    const response = await this.api.put(`${this.route}${selector.id}`, body);
    return response.data;
  }

  async updatePreSaleQuantity(
    selector: Nomalism.shared.IFindByIdRequest,
    body: IUpdatePreSaleQuantityRequest,
  ): Promise<IFindResponse | null> {
    const response = await this.api.put(`${this.route}${selector.id}/quantity`, body);
    return response.data;
  }

  async removeProduct(
    selector: Nomalism.shared.IFindByIdRequest,
    body: IRemovePreSaleProductRequest,
  ): Promise<void> {
    const response = await this.api.put(`${this.route}product/${selector.id}`, body);
    return response.data;
  }

  async importToClientProposal(data: IImportToClientProposalRequest): Promise<void> {
    const response = await this.api.post(`${this.route}client_proposal`, data);
    return response.data;
  }
}
