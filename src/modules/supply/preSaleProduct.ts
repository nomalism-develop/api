import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import IFindResponse = Nomalism.PreSaleProduct.Entity;

export default class Repository implements Nomalism.PreSaleProduct.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async find(): Promise<IFindResponse[]> {
    const response = await this.api.get(`${this.route}`);
    return response.data;
  }

  async findById(selector: Nomalism.shared.IFindByIdRequest): Promise<IFindResponse | null> {
    const response = await this.api.get(`${this.route}${selector.id}`);
    return response.data;
  }
}
