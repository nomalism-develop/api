import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import IEntity = Nomalism.ProviderCurrentAccount.Entity;

export import IFindRequest = Nomalism.ProviderCurrentAccount.IFindRequest;

export default class Repository implements Nomalism.ProviderCurrentAccount.IApi {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async findByOwnerId(params: IFindRequest): Promise<Nomalism.shared.IPaginationResponse<IEntity>> {
    const response = await this.api.get(`${this.route}by_owner`, {
      params,
    });
    return response.data;
  }
}
