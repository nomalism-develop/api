import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export type IEntity = Nomalism.StartDocumentHeaderLastUpdate.Entity;

export default class Repository implements Nomalism.StartDocumentHeaderLastUpdate.IController {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async findById(selector: Nomalism.shared.IFindByIdRequest): Promise<IEntity | null> {
    const response = await this.api.get(`${this.route}${selector.id}`);
    return response.data;
  }
}
