import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import IEntity = Nomalism.VatValidation.Entity;

export default class Repository implements Nomalism.VatValidation.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async findByLatest(nif: string): Promise<IEntity | null> {
    const response = await this.api.get(`${this.route}`, {
      params: {
        nif,
      },
    });
    return response.data;
  }
}
