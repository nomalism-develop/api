import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import IEntity = Nomalism.Workflow.Entity;

export import IFindRequest = Nomalism.Workflow.IFindRequest;

export import IFindByOwnerIdRequest = Nomalism.Workflow.IFindByOwnerIdRequest;
export import IFindByOwnerIdResponse = Nomalism.Workflow.IFindByOwnerIdResponse;

export import ICreateRequest = Nomalism.Workflow.ICreateRequest;

export import IUpdateRequest = Nomalism.Workflow.IUpdateRequest;

export import IFindByQueryRequest = Nomalism.Workflow.IFindByQueryRequest;
export type IFindByQueryResponse = Nomalism.shared.IPaginationResponse<IEntity>;

export default class Repository implements Nomalism.Workflow.IApi {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async find(params: IFindRequest): Promise<IFindByOwnerIdResponse[]> {
    const response = await this.api.get(`${this.route}`, { params });
    return response.data;
  }

  async findByOwnerId(params: IFindByOwnerIdRequest): Promise<IFindByOwnerIdResponse[]> {
    const response = await this.api.get(`${this.route}by_owner`, { params });
    return response.data;
  }

  async create(body: ICreateRequest): Promise<IFindByOwnerIdResponse> {
    const response = await this.api.post(`${this.route}`, body);
    return response.data;
  }

  async update(
    selector: Nomalism.shared.IFindByIdNumberRequest,
    body: IUpdateRequest,
  ): Promise<IFindByOwnerIdResponse | null> {
    const response = await this.api.put(`${this.route}${selector.id}`, body);
    return response.data;
  }

  async deleteOne(
    selector: Nomalism.shared.IFindByIdNumberRequest,
  ): Promise<IFindByOwnerIdResponse | null> {
    const response = await this.api.delete(`${this.route}${selector.id}`);
    return response.data;
  }
}
