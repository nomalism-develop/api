import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import IEntity = Nomalism.Channel.Entity;

export import IChannel = Nomalism.Channel.IChannel;

export import ICreateRequest = Nomalism.Channel.ICreateRequest;
export import IUpdateRequest = Nomalism.Channel.IUpdateRequest;
export import IFindMinifiedRequest = Nomalism.Channel.IFindMinifiedRequest;

export default class Repository implements Nomalism.Channel.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async create(body: ICreateRequest): Promise<IEntity> {
    const response = await this.api.post(`${this.route}`, body);
    return response.data;
  }

  async find(): Promise<IEntity[]> {
    const response = await this.api.get(`${this.route}`);
    return response.data;
  }

  async findMinified(params?: IFindMinifiedRequest): Promise<IEntity[]> {
    const response = await this.api.get(`${this.route}minified`, {
      params,
    });
    return response.data;
  }

  async findById(id: Nomalism.shared.IFindByIdNumberRequest): Promise<IEntity | null> {
    const response = await this.api.get(`${this.route}${id}`);
    return response.data;
  }

  async update(id: Nomalism.shared.IFindByIdNumberRequest, body: IUpdateRequest): Promise<void> {
    await this.api.post(`${this.route}${id}`, body);
  }

  async deleteOne(id: Nomalism.shared.IFindByIdNumberRequest): Promise<void> {
    await this.api.delete(`${this.route}${id}`);
  }
}
