import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import IEntity = Nomalism.CLT.Entity;

export import ICreateRequest = Nomalism.CLT.ICreateRequest;
export import IUpdateRequest = Nomalism.CLT.IUpdateRequest;
export import IFindMinifiedRequest = Nomalism.CLT.IFindMinifiedRequest;

export default class Repository implements Nomalism.CLT.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async create(body: ICreateRequest): Promise<IEntity> {
    const response = await this.api.post(`${this.route}`, body);
    return response.data;
  }

  async find(): Promise<IEntity[]> {
    const response = await this.api.get(`${this.route}`);
    return response.data;
  }

  async findMinified(params?: IFindMinifiedRequest): Promise<IEntity[]> {
    const response = await this.api.get(`${this.route}minified`, {
      params,
    });
    return response.data;
  }

  async findById(id: Nomalism.shared.IFindByIdNumberRequest): Promise<IEntity | null> {
    const response = await this.api.get(`${this.route}${id}`);
    return response.data;
  }

  async update(id: Nomalism.shared.IFindByIdNumberRequest, body: IUpdateRequest): Promise<void> {
    await this.api.post(`${this.route}${id}`, body);
  }

  async deleteOne(id: Nomalism.shared.IFindByIdNumberRequest): Promise<void> {
    await this.api.delete(`${this.route}${id}`);
  }
}
