import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import IEntity = Nomalism.Tickets.Entity;

export import ITicketCreateRequest = Nomalism.Tickets.ITicketCreateRequest;
export import ITicketCreateResponse = Nomalism.Tickets.ITicketCreateResponse;

export import ITicketUpdateRequest = Nomalism.Tickets.ITicketUpdateRequest;
export import ITicketUpdateResponse = Nomalism.Tickets.ITicketUpdateResponse;

export import ITicketUndoResponse = Nomalism.Tickets.ITicketUndoResponse;

export import ITicketsFindTodayResponse = Nomalism.Tickets.ITicketsFindTodayResponse;

export default class Repository implements Nomalism.Tickets.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async create(body: ITicketCreateRequest): Promise<ITicketCreateResponse> {
    const response = await this.api.post(`${this.route}`, body);
    return response.data;
  }

  async findToday(): Promise<ITicketsFindTodayResponse> {
    const response = await this.api.get(`${this.route}find_today`);
    return response.data;
  }

  async update(body: ITicketUpdateRequest): Promise<ITicketUpdateResponse> {
    const response = await this.api.put(`${this.route}`, body);
    return response.data;
  }

  async undoLastCall(): Promise<ITicketUndoResponse> {
    const response = await this.api.put(`${this.route}undo_last_call`);
    return response.data;
  }
}
