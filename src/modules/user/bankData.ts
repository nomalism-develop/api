import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import IFindRequest = Nomalism.BankData.IFindRequest;
export import IFindResponse = Nomalism.BankData.IFindResponse;

export import IFindPaginatedRequest = Nomalism.BankData.IFindPaginatedRequest;

export import IFindByOwnerIdRequest = Nomalism.shared.IFindByOwnerIdRequest;
export import IFindByOwnerIdResponse = Nomalism.BankData.IFindByOwnerIdResponse;

export import ICreateRequest = Nomalism.BankData.ICreateRequest;

export import IEntity = Nomalism.BankData.Entity;

export import IUpdateRequest = Nomalism.BankData.IUpdateRequest;
export type IPaginationResponse =
  Nomalism.shared.IPaginationResponse<Nomalism.BankData.IFindResponse>;

export default class Repository implements Nomalism.BankData.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async find(params: IFindRequest): Promise<IFindResponse[]> {
    const response = await this.api.get(`${this.route}`, { params });
    return response.data;
  }

  async findPaginated(params: IFindPaginatedRequest): Promise<IPaginationResponse> {
    const response = await this.api.get(`${this.route}paginated`, { params });
    return response.data;
  }

  async findByOwnerId(params: IFindByOwnerIdRequest): Promise<IFindByOwnerIdResponse[]> {
    const response = await this.api.get(`${this.route}by_owner`, {
      params,
    });
    return response.data;
  }

  async findById(selector: Nomalism.shared.IFindByIdRequest): Promise<IFindResponse | null> {
    const response = await this.api.get(`${this.route}${selector.id}`);
    return response.data;
  }

  async create(body: ICreateRequest): Promise<IFindByOwnerIdResponse> {
    const response = await this.api.post(`${this.route}`, body);
    return response.data;
  }

  async update(
    selector: Nomalism.shared.IFindByIdRequest,
    body: IUpdateRequest,
  ): Promise<IFindByOwnerIdResponse | null> {
    const response = await this.api.put(`${this.route}${selector.id}`, body);
    return response.data;
  }

  async deleteOne(
    selector: Nomalism.shared.IFindByIdRequest,
  ): Promise<IFindByOwnerIdResponse | null> {
    const response = await this.api.delete(`${this.route}${selector.id}`);
    return response.data;
  }
}
