import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import IFindOneRequest = Nomalism.Favorites.IFindOneRequest;

export import ICreateRequest = Nomalism.Favorites.ICreateRequest;
export import IDeleteRequest = Nomalism.Favorites.IDeleteRequest;

export import IEntity = Nomalism.Favorites.Entity;

export default class Repository implements Nomalism.Favorites.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async findByUrl(params: IFindOneRequest): Promise<boolean> {
    const response = await this.api.get(`${this.route}`, { params });
    return response.data;
  }

  async list(): Promise<IEntity[]> {
    const response = await this.api.get(`${this.route}list`);
    return response.data;
  }

  async create(body: ICreateRequest): Promise<IEntity> {
    const response = await this.api.post(`${this.route}`, body);
    return response.data;
  }

  async deleteOne(params: IDeleteRequest): Promise<IEntity | null> {
    const response = await this.api.delete(`${this.route}`, { params });
    return response.data;
  }
}
