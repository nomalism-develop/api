import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export default class Repository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async logout(): Promise<void> {
    await this.api.post(`${this.route}`);
  }
}
