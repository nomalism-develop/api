import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import IForgotPassword = Nomalism.Password.IForgotPassword;

export import IResetPassword = Nomalism.Password.IResetPassword;

export default class Repository implements Nomalism.Password.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async forgotPassword(body: IForgotPassword): Promise<void> {
    await this.api.post(`${this.publicRoute}forgot`, body);
  }

  async resetPassword(body: IResetPassword): Promise<void> {
    await this.api.post(`${this.publicRoute}reset`, body);
  }
}
