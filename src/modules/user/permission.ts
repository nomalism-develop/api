import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import IFindMinifiedRequest = Nomalism.shared.IFindMinifiedRequest;
export import IFindMinifiedResponse = Nomalism.shared.IFindMinifiedResponse;

export import ICreateRequest = Nomalism.Permissions.ICreateRequest;
export import IEntity = Nomalism.Permissions.Entity;

export default class Repository implements Nomalism.Permissions.IApi {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async find(): Promise<Nomalism.Permissions.Entity[]> {
    const response = await this.api.get(`${this.route}`);
    return response.data;
  }

  async findMinified(params?: IFindMinifiedRequest): Promise<IFindMinifiedResponse[]> {
    const response = await this.api.get(`${this.route}minified`, {
      params,
    });
    return response.data;
  }

  async create(body: ICreateRequest): Promise<IEntity> {
    const response = await this.api.post(`${this.route}`, body);
    return response.data;
  }

  async deleteOne(selector: Nomalism.shared.IFindByIdNumberRequest): Promise<IEntity | null> {
    const response = await this.api.delete(`${this.route}${selector.id}`);
    return response.data;
  }
}
