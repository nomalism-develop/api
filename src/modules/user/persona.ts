import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import IEntityExtended = Nomalism.Persona.IEntityExtended;
export import IFindByOwnerIdRequest = Nomalism.shared.IFindByOwnerIdRequest;
export import ICreateRequest = Nomalism.Persona.ICreateRequest;
export import IUpdateRequest = Nomalism.Persona.IUpdateRequest;

export import IBasicSearchRequest = Nomalism.Persona.IBasicSearchRequest;
export import IBasicSearchResponse = Nomalism.Persona.IBasicSearchResponse;

export import IFindByOwnerIdResponse = Nomalism.Persona.IFindByOwnerIdResponse;

export import IFindRequest = Nomalism.Persona.IFindRequest;
export import IFindResponse = Nomalism.Persona.IFindResponse;

export default class Repository implements Nomalism.Persona.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async findByOwnerId(params: IFindByOwnerIdRequest): Promise<IFindByOwnerIdResponse[]> {
    const response = await this.api.get(`${this.route}by_owner`, {
      params,
    });
    return response.data;
  }

  async findById(selector: Nomalism.shared.IFindByIdRequest): Promise<IEntityExtended | null> {
    const response = await this.api.get(`${this.route}${selector.id}`);
    return response.data;
  }

  async findContactPersonaByOwnerId(
    params: Nomalism.shared.IFindByOwnerIdRequest,
  ): Promise<Nomalism.Persona.IFindContactPersonaByOwnerId> {
    const response = await this.api.get(`${this.route}contact`, { params });
    return response.data;
  }

  async create(body: ICreateRequest): Promise<IEntityExtended> {
    const response = await this.api.post(`${this.route}`, body);
    return response.data;
  }

  async update(
    selector: Nomalism.shared.IFindByIdRequest,
    body: IUpdateRequest,
  ): Promise<IEntityExtended | null> {
    const response = await this.api.put(`${this.route}${selector.id}`, body);
    return response.data;
  }

  async deleteOne(params: Nomalism.Persona.IDeletePersonaRequest): Promise<IEntityExtended | null> {
    const response = await this.api.delete(`${this.route}`, { params });
    return response.data;
  }

  async findByBasic(
    params: IBasicSearchRequest,
  ): Promise<Nomalism.shared.IPaginationResponse<IBasicSearchResponse>> {
    const response = await this.api.get(`${this.route}basic_search`, {
      params,
    });
    return response.data;
  }

  async find(data: IFindRequest): Promise<IFindResponse[]> {
    const response = await this.api.get(`${this.route}`, {
      data,
    });
    return response.data;
  }

  async findByEmail(
    data: Nomalism.Persona.IFindByEmailRequest,
  ): Promise<Nomalism.Persona.IFindByEmailResponse[]> {
    const response = await this.api.get(`${this.route}by_email`, {
      data,
    });
    return response.data;
  }
}
