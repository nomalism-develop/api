import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import IFindRequest = Nomalism.ReasonForExemption.IFindRequest;
export import IFindResponse = Nomalism.ReasonForExemption.IFindResponse;

export import IFindPaginatedRequest = Nomalism.ReasonForExemption.IFindPaginatedRequest;

export import IFindMinifiedRequest = Nomalism.shared.IFindMinifiedRequest;
export import IFindMinifiedResponse = Nomalism.shared.IFindMinifiedResponse;

export import ICreateRequest = Nomalism.ReasonForExemption.ICreateRequest;
export import IEntity = Nomalism.ReasonForExemption.Entity;

export import IUpdateRequest = Nomalism.ReasonForExemption.IUpdateRequest;
export type IPaginationResponse =
  Nomalism.shared.IPaginationResponse<Nomalism.ReasonForExemption.IFindResponse>;

export default class Repository implements Nomalism.ReasonForExemption.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async find(params: IFindRequest): Promise<IFindResponse[]> {
    const response = await this.api.get(`${this.route}`, { params });
    return response.data;
  }

  async findPaginated(params: IFindPaginatedRequest): Promise<IPaginationResponse> {
    const response = await this.api.get(`${this.route}paginated`, { params });
    return response.data;
  }

  async findMinified(params?: IFindMinifiedRequest): Promise<IFindMinifiedResponse[]> {
    const response = await this.api.get(`${this.route}minified`, {
      params,
    });
    return response.data;
  }

  async findById(selector: Nomalism.shared.IFindByIdRequest): Promise<IFindResponse | null> {
    const response = await this.api.get(`${this.route}${selector.id}`);
    return response.data;
  }

  async create(body: ICreateRequest): Promise<IEntity> {
    const response = await this.api.post(`${this.route}`, body);
    return response.data;
  }

  async update(
    selector: Nomalism.shared.IFindByIdRequest,
    body: IUpdateRequest,
  ): Promise<IEntity | null> {
    const response = await this.api.put(`${this.route}${selector.id}`, body);
    return response.data;
  }

  async deleteOne(selector: Nomalism.shared.IFindByIdRequest): Promise<IEntity | null> {
    const response = await this.api.delete(`${this.route}${selector.id}`);
    return response.data;
  }
}
