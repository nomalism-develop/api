import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import ICreateRequest = Nomalism.Sessions.ICreateRequest;
export import ICreateResponse = Nomalism.Sessions.ICreateResponse;

export default class Repository implements Nomalism.Sessions.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async create(body: ICreateRequest): Promise<ICreateResponse> {
    const response = await this.api.post(`${this.publicRoute}`, body);
    return response.data;
  }
}
