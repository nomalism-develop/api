import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import IFindRequest = Nomalism.StoreOperator.IFindRequest;
export import IFindResponse = Nomalism.StoreOperator.IFindResponse;

export import IFindPaginatedRequest = Nomalism.StoreOperator.IFindPaginatedRequest;

export import IFindMinifiedRequest = Nomalism.shared.IFindMinifiedRequest;
export import IFindMinifiedResponse = Nomalism.shared.IFindMinifiedResponse;

export import IFindByOwnerIdRequest = Nomalism.shared.IFindByOwnerIdRequest;
export import IFindByOwnerIdResponse = Nomalism.StoreOperator.IFindByOwnerIdResponse;

export import IFindByIdResponse = Nomalism.StoreOperator.IFindByIdResponse;

export import ICreateRequest = Nomalism.StoreOperator.ICreateRequest;

export import IEntity = Nomalism.StoreOperator.Entity;

export import IUpdateRequest = Nomalism.StoreOperator.IUpdateRequest;

export import ICheckPinRequest = Nomalism.StoreOperator.ICheckPinRequest;
export type IPaginationResponse =
  Nomalism.shared.IPaginationResponse<Nomalism.StoreOperator.IFindResponse>;

export default class Repository implements Nomalism.StoreOperator.IApi {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async find(params: IFindRequest): Promise<IFindResponse[]> {
    const response = await this.api.get(`${this.route}`, { params });
    return response.data;
  }

  async findPaginated(params: IFindPaginatedRequest): Promise<IPaginationResponse> {
    const response = await this.api.get(`${this.route}paginated`, { params });
    return response.data;
  }

  async findMinified(params?: IFindMinifiedRequest): Promise<IFindMinifiedResponse[]> {
    const response = await this.api.get(`${this.route}minified`, {
      params,
    });
    return response.data;
  }

  async findByOwnerId(params: IFindByOwnerIdRequest): Promise<IFindByOwnerIdResponse[]> {
    const response = await this.api.get(`${this.route}by_owner`, {
      params,
    });
    return response.data;
  }

  async findById(selector: Nomalism.shared.IFindByIdRequest): Promise<IFindByIdResponse | null> {
    const response = await this.api.get(`${this.route}${selector.id}`);
    return response.data;
  }

  async create(body: ICreateRequest): Promise<IFindResponse> {
    const response = await this.api.post(`${this.route}`, body);
    return response.data;
  }

  async update(
    selector: Nomalism.shared.IFindByIdRequest,
    body: IUpdateRequest,
  ): Promise<IFindResponse | null> {
    const response = await this.api.put(`${this.route}${selector.id}`, body);
    return response.data;
  }

  async deleteOne(selector: Nomalism.shared.IFindByIdRequest): Promise<IFindResponse | null> {
    const response = await this.api.delete(`${this.route}${selector.id}`);
    return response.data;
  }

  async checkPin(data: ICheckPinRequest): Promise<IFindResponse> {
    const response = await this.api.post(`${this.route}pin`, data);
    return response.data;
  }

  async updateStoreOperator(
    selector: Nomalism.shared.IFindByIdRequest,
    body: Nomalism.StoreOperator.IStoreOperatorUpdateRequest,
  ): Promise<void> {
    await this.api.put(`${this.route}store_operator${selector.id}`, body);
  }
}
