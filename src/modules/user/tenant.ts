import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import IEntity = Nomalism.Tenant.Entity;

export import ICreateRequest = Nomalism.Tenant.ICreateRequest;
export import IUpdateRequest = Nomalism.Tenant.IUpdateRequest;

export default class Repository implements Nomalism.Tenant.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async create(body: ICreateRequest): Promise<IEntity> {
    const response = await this.api.post(`${this.route}`, body);
    return response.data;
  }

  async find(): Promise<IEntity[]> {
    const response = await this.api.get(`${this.route}`);
    return response.data;
  }

  async findById(selector: Nomalism.shared.IFindByIdRequest): Promise<IEntity | null> {
    const response = await this.api.get(`${this.route}${selector.id}`);
    return response.data;
  }

  async update(
    selector: Nomalism.shared.IFindByIdRequest,
    body: IUpdateRequest,
  ): Promise<IEntity | null> {
    const response = await this.api.put(`${this.route}${selector.id}`, body);
    return response.data;
  }
}
