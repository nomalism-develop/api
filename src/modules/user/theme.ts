import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import IEntity = Nomalism.Theme.Entity;

export import createOrUpdate = Nomalism.Theme.ICreateOrUpdateRequest;

export default class Repository implements Nomalism.Theme.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async createOrUpdate(body: createOrUpdate): Promise<void> {
    await this.api.post(`${this.route}`, body);
  }
}
