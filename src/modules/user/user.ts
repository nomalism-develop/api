import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import IFindRequest = Nomalism.Users.IFindRequest;
export import IFindResponse = Nomalism.Users.IFindResponse;

export import IFindPaginatedRequest = Nomalism.Users.IFindPaginatedRequest;

export import IFindMinifiedRequest = Nomalism.shared.IFindMinifiedRequest;
export import IFindMinifiedResponse = Nomalism.shared.IFindMinifiedResponse;

export import IFindByIdResponse = Nomalism.Users.IFindByIdResponse;

export import IFindProvidersWithClientIdRequest = Nomalism.Users.IFindProvidersWithClientIdRequest;
export import IFindProvidersWithClientIdResponse = Nomalism.Users.IFindProvidersWithClientIdResponse;

export import ICreateRequest = Nomalism.Users.ICreateRequest;

export import IEntity = Nomalism.Users.Entity;

export import IUpdateRequest = Nomalism.Users.IUpdateRequest;
export import IFindFromClientOrProviderRequest = Nomalism.Users.IFindFromClientOrProviderRequest;
export import IFindFromClientOrProviderResponse = Nomalism.Users.IFindFromClientOrProviderResponse;
export type IPaginationResponse = Nomalism.shared.IPaginationResponse<Nomalism.Users.IFindResponse>;

export default class Repository implements Nomalism.Users.IApi {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async find(params: IFindRequest): Promise<IFindResponse[]> {
    const response = await this.api.get(`${this.route}`, { params });
    return response.data;
  }

  async findPaginated(params: IFindPaginatedRequest): Promise<IPaginationResponse> {
    const response = await this.api.get(`${this.route}paginated`, { params });
    return response.data;
  }

  async findMinified(params?: IFindMinifiedRequest): Promise<IFindMinifiedResponse[]> {
    const response = await this.api.get(`${this.route}minified`, {
      params,
    });
    return response.data;
  }

  async findById(selector: Nomalism.shared.IFindByIdRequest): Promise<IFindByIdResponse | null> {
    const response = await this.api.get(`${this.route}${selector.id}`);
    return response.data;
  }

  async create(body: ICreateRequest): Promise<IFindResponse> {
    const response = await this.api.post(`${this.route}`, body);
    return response.data;
  }

  async update(
    selector: Nomalism.shared.IFindByIdRequest,
    body: IUpdateRequest,
  ): Promise<IFindResponse | null> {
    const response = await this.api.put(`${this.route}${selector.id}`, body);
    return response.data;
  }

  async deleteOne(selector: Nomalism.shared.IFindByIdRequest): Promise<IFindResponse | null> {
    const response = await this.api.delete(`${this.route}${selector.id}`);
    return response.data;
  }

  async findProvidersWithClientId(
    body: IFindProvidersWithClientIdRequest,
  ): Promise<IFindProvidersWithClientIdResponse[]> {
    const response = await this.api.post(`${this.route}providers`, body);
    return response.data;
  }

  async findFromClientOrProviderId(
    body: IFindFromClientOrProviderRequest,
  ): Promise<IFindFromClientOrProviderResponse[]> {
    const response = await this.api.post(`${this.route}from_client_or_provider_id`, body);
    return response.data;
  }
}
