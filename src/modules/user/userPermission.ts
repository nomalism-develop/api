import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import IFindByOwnerIdRequest = Nomalism.shared.IFindByOwnerIdRequest;
export import IFindByOwnerIdResponse = Nomalism.UserPermissions.IFindByOwnerIdResponse;

export import ICreateRequest = Nomalism.UserPermissions.ICreateRequest;

export import IEntity = Nomalism.UserPermissions.Entity;

export default class Repository implements Nomalism.UserPermissions.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async findByOwnerId(params: IFindByOwnerIdRequest): Promise<IFindByOwnerIdResponse[]> {
    const response = await this.api.get(`${this.route}by_owner`, {
      params,
    });
    return response.data;
  }

  async create(body: ICreateRequest): Promise<IFindByOwnerIdResponse> {
    const response = await this.api.post(`${this.route}`, body);
    return response.data;
  }

  async deleteOne(
    selector: Nomalism.shared.IFindByIdNumberRequest,
  ): Promise<IFindByOwnerIdResponse | null> {
    const response = await this.api.delete(`${this.route}${selector.id}`);
    return response.data;
  }
}
