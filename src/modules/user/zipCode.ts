import Nomalism from '@nomalism-com/types';
import { AxiosInstance } from 'axios';
import { IModuleConstructor } from '../../main';

export import IEntity = Nomalism.ZipCode.Entity;

export import ICreateRequest = Nomalism.ZipCode.ICreateRequest;
export import IFindByPostalCode = Nomalism.ZipCode.IFindByPostalCode;

export default class Repository implements Nomalism.ZipCode.IRepository {
  public route: string;

  public publicRoute: string;

  private api: AxiosInstance;

  constructor({ api, route, publicRoute }: IModuleConstructor) {
    this.api = api;
    this.route = route;
    this.publicRoute = publicRoute;
  }

  async create(body: ICreateRequest[]): Promise<void> {
    await this.api.post(`${this.route}`, body);
  }

  async findByPostalCode(params: IFindByPostalCode): Promise<IEntity[]> {
    const response = await this.api.get(`${this.route}`, { params });
    return response.data;
  }
}
